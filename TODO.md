TO DO List

* Better EoL handling
	* Allow compiling sources from any O.S., and not just from the current one.
* Extend (and fix) the documentation
* Ressurrect features from SJASM 0.39 (http://xl2s.eu.pn/sjasm.html)
	* ARM
	* THUMBS
* Backport fixes and enhancements from SJASM 0.42 (http://xl2s.eu.pn/sjasm.html)
* Native (direct) Support for:
	* CCE MC-1000
	* MSX (from SjASM 0.42)
	* TRS-80 Model I (as soon as I get a machine to play)
	* CPC (if I manage to get help from CPC owners)
* Clean up
	* Drop support for Russian language (I don't know how to maintain it)
	* (correctly =]) Getting rid of the GCC warnings on building.
	* drop support for ANSI C. Keep everything under C++ and STDLIB.
		* We already migrated a lot of code to C++, there's no point on keeping ANSI C compatibility. 
* Merge Review
	* vitamin-caig 
		+ commit eea231d58b55de4d7eef892ec924baea3dc4f6fe (Get rid of MAP/ENDMAP) was not merged
			- Legacy Code matters!
			- But this need to be checked as the code trees walks away from each other...
	* konamiman
		+ COMPASS merges need to be tested.
			- COMPASS code **should** fail in normal mode! 
		+ This merge can play havoc with the whole parser! 
* Unit Tests
	* Yes, we need Unit Tests for everything! 
* Update documentation
	* Get rid of the XML, and adopt MarkDown for everything. 
* Additional "alien" syntax support:
	* [pasmo](http://pasmo.speccy.org)
	* M80 do CP/M
		+ Vai dar trabalho, é outra sintaxe!  
	* SLR Z80 da SLR Systems (Z80ASM)
	* RASM (for CPC, worths a look).
		+ http://www.cpcwiki.eu/forum/programming/rasm-z80-assembler-in-beta/

