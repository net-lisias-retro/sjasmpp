/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// sjasmce.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "sjasmce.h"
#include <windows.h>
#include "sjdefs.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void (*WriteUnicodeString)(wchar_t* String);
void (*WriteEOF)(void);

int SJASMCompileFiles(int argc, wchar_t* argv[]) {
	/*WriteUnicodeString(L"START");
	_TCHAR* files[2];
	files[0] = _totchar("programname");
	files[1] = _totchar("\\My Documents\\SjASMPlus\\test.asm");
	main(2, files);
	WriteUnicodeString(L"STOP");*/

	/*for (int i=0;i<argc;i++) {

	}*/
	main(argc, argv);
	return 0;
}

void SJASMConsoleWriteUnicodeStringCallback(void (*Function)(wchar_t*)) {
	WriteUnicodeString = Function;
}

void SJASMConsoleWriteEOFCallback(void (*Function)(void)) {
	WriteEOF = Function;
}

void WriteOutput(char Char) {
	WriteUnicodeString(_totchar(&Char));
}
void WriteOutput(char* String) {
	WriteUnicodeString(_totchar(String));
}
void WriteOutput(_TCHAR* String) {
	WriteUnicodeString(String);
}
void WriteOutput(int Number) {
	char String[35];
	_itoa(Number, String, 10);
	WriteUnicodeString(_totchar(String));
}
void WriteOutput(unsigned char Number) {
	char String[35];
	_ultoa((unsigned long)Number, String, 10);
	WriteUnicodeString(_totchar(String));
}
void WriteOutput(long Number) {
	char String[35];
	_ltoa(Number, String, 10);
	WriteUnicodeString(_totchar(String));
}
void WriteOutput(unsigned long Number) {
	char String[35];
	_ultoa(Number, String, 10);
	WriteUnicodeString(_totchar(String));
}
void WriteOutput(float Number) {
	char String[35];
	_gcvt(Number, 10, String);
	WriteUnicodeString(_totchar(String));
}
void WriteOutputEOF() {
	WriteEOF();
}


