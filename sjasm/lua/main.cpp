/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a product,
     an acknowledgment in the product documentation would be appreciated but is
     not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "main.h"

#include "sjerr.h"
#include "sjasm.h"
#include "support.h"
#include "sjio.h"

lua_State *LUA;
int LuaLine=-1;

void ErrorFromLua(char const * fout, char const * bd, int type) {
	char * const ep = ErrorLine;

	++ErrorCount;
	{
		char count[25] = "";
		SPRINTF1(count, 25, "%i", ErrorCount);
		DefineTable.Replace("_ERRORS", count);
	}

	{
		lua_Debug ar;

		lua_getstack(LUA, 1, &ar) ;
		lua_getinfo(LUA, "l", &ar);
		int const ln = LuaLine + ar.currentline;
		SPRINTF3(ep, LINEMAX2, "%s(%i): error: %s", filename, ln, fout);
	}

	if (bd)						STRCAT(ep, LINEMAX2, ": "); STRCAT(ep, LINEMAX2, bd);
	if (!strchr(ep, '\n')) 		STRCAT(ep, LINEMAX2, "\n");
	if (FP_ListingFile != NULL)	fputs(ErrorLine, FP_ListingFile);

	_CERR ErrorLine _END;
	ExitASM(ERR_INTERNAL);
}

void WarningFromLua(char const * fout, char const * bd, int type) {
	char * const ep = ErrorLine;

	++WarningCount;
	{
		char count[25] = "";
		SPRINTF1(count, 25, "%i", WarningCount);
		DefineTable.Replace("_WARNINGS", count);
	}

	{
		lua_Debug ar;

		lua_getstack(LUA, 1, &ar) ;
		lua_getinfo(LUA, "l", &ar);
		int const ln = LuaLine + ar.currentline;
		SPRINTF3(ep, LINEMAX2, "%s(%i): warning: %s", filename, ln, fout);
	}

	if (bd) {
		STRCAT(ep, LINEMAX2, ": ");
		STRCAT(ep, LINEMAX2, bd);
	}

	if (!strchr(ep, '\n'))		STRCAT(ep, LINEMAX2, "\n");
	if (FP_ListingFile != NULL)	fputs(ErrorLine, FP_ListingFile);

	_CERR ErrorLine _END;
}

void LuaFatalError(lua_State *L) {
	Error((char *)lua_tostring(L, -1), 0, FATAL, ERR_INTERNAL);
}

void LuaShellExec(char *command) {
#ifdef WIN32

	WinExec(command, SW_SHOWNORMAL);
#else
	system(command);
#endif
}

namespace lua {

void Init() {
	LUA = lua_open();
	lua_atpanic(LUA, (lua_CFunction)LuaFatalError);
	luaL_openlibs(LUA);
	luaopen_pack(LUA);

	tolua_sjasm_open(LUA);
}

}

