/*
** Lua binding: sjasm
** Generated automatically by tolua++-1.0.92 on 11/06/08 00:50:38.
*/

#ifndef __LUA_BINDINGS_H__
#define __LUA_BINDINGS_H__

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "tolua++.h"
}

/* Exported function */
TOLUA_API int  tolua_sjasm_open (lua_State* tolua_S);

#endif //__LUA_BINDINGS_H__
