/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a product,
     an acknowledgment in the product documentation would be appreciated but is
     not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "directives.h"

#include "sjerr.h"
#include "main.h"
#include "support.h"
#include "bindings.h"
#include "sjio.h"
#include "reader.h"

extern CFunctionTable DirectivesTable;

void _lua_showerror() {
	int ln;

	// part from Error(...)
	char *err = STRDUP(lua_tostring(LUA, -1));
	if (err == NULL) {
		Error("No enough memory!", 0, FATAL, ERR_RUNTIME);
	}
	//_COUT err _ENDL;
	err += 18;
	char *pos = strstr(err, ":");
	//_COUT err _ENDL;
	//_COUT pos _ENDL;
	*(pos++) = 0;
	//_COUT err _ENDL;
	ln = atoi(err) + LuaLine;

	// print error and other actions
	err = ErrorLine;
	SPRINTF3(err, LINEMAX2, "%s(%i): error: [LUA]%s", filename, ln, pos);

	if (!strchr(err, '\n')) {
		STRCAT(err, LINEMAX2, "\n");
	}

	if (FP_ListingFile != NULL) {
		fputs(ErrorLine, FP_ListingFile);
	}
	_CERR ErrorLine _END;

	PreviousErrorLine = ln;

	ErrorCount++;

	char count[25];
	SPRINTF1(count, 25, "%i", ErrorCount);
	DefineTable.Replace("_ERRORS", count);
	// end Error(...)

	lua_pop(LUA, 1);
}

typedef struct luaMemFile
{
  const char *text;
  size_t size;
} luaMemFile;

const char *readMemFile(lua_State *, void *ud, size_t *size)
{
  // Convert the ud pointer (UserData) to a pointer of our structure
  luaMemFile *luaMF = (luaMemFile *) ud;

  // Are we done?
  if(luaMF->size == 0)
	return NULL;

  // Read everything at once
  // And set size to zero to tell the next call we're done
  *size = luaMF->size;
  luaMF->size = 0;

  // Return a pointer to the readed text
  return luaMF->text;
}

static void dirLUA() {
	int error;
	char *rp, *id;
	char *buff = new char[32768];
	char *bp=buff;
	char size=0;
	int ln=0;
	bool execute=false;

	luaMemFile luaMF;

	SkipBlanks();

	if ((id = GetID(lp)) && strlen(id) > 0) {
		if (cmphstr(id, "pass1")) {
			if (pass == 1) {
				execute = true;
			}
		} else if (cmphstr(id, "pass2")) {
			if (pass == 2) {
				execute = true;
			}
		} else if (cmphstr(id, "pass3")) {
			if (pass == 3) {
				execute = true;
			}
		} else if (cmphstr(id, "allpass")) {
			execute = true;
		} else {
			//_COUT id _CMDL "A" _ENDL;
			Error("[LUA] Syntax error", id);
		}
	} else if (pass == LASTPASS) {
		execute = true;
	}

	ln = CurrentLocalLine;
	ListFile();
	while (1) {
		if (!ReadLine(false)) {
			Error("[LUA] Unexpected end of lua script", 0, PASS3); break;
		}
		lp = line;
		rp = line;
		SkipBlanks(rp);
		if (cmphstr(rp, "endlua")) {
			if (execute) {
				if ((bp-buff) + (rp-lp-6) < 32760 && (rp-lp-6) > 0) {
					STRNCPY(bp, 32768-(bp-buff)+1, lp, rp-lp-6);
					bp += rp-lp-6;
					*(bp++) = '\n';
					*(bp) = 0;
				} else {
					Error("[LUA] Maximum size of Lua script is 32768 bytes", 0, FATAL, ERR_RUNTIME);
					return;
				}
			}
			lp = rp;
			break;
		}
		if (execute) {
			if ((bp-buff) + strlen(lp) < 32760) {
				STRCPY(bp, 32768-(bp-buff)+1, lp);
				bp += strlen(lp);
				*(bp++) = '\n';
				*(bp) = 0;
			} else {
				Error("[LUA] Maximum size of Lua script is 32768 bytes", 0, FATAL, ERR_RUNTIME);
				return;
			}
		}

		ListFileSkip(line);
	}

	if (execute) {
		LuaLine = ln;
		luaMF.text = buff;
		luaMF.size = strlen(luaMF.text);
		error = lua_load(LUA, readMemFile, &luaMF, "script") || lua_pcall(LUA, 0, 0, 0);
		//error = luaL_loadbuffer(LUA, (char*)buff, sizeof(buff), "script") || lua_pcall(LUA, 0, 0, 0);
		//error = luaL_loadstring(LUA, buff) || lua_pcall(LUA, 0, 0, 0);
		if (error) {
			_lua_showerror();
		}
		LuaLine = -1;
	}

	delete[] buff;
}

static void dirENDLUA() {
	Error("[ENDLUA] End of lua script without script", 0);
}

static void dirINCLUDELUA() {
	if (pass != 1) {
		return;
	}
	int error;

	bool search_local_first;
	Filename const f = GetFileName(lp, search_local_first);
	Filename const fnaam = Filename(GetPath(f.c_str(), search_local_first));

	if (!FileExists(fnaam.c_str())) {
		Error("[INCLUDELUA] File doesn't exist", fnaam.c_str(), PASS1);
		return;
	}

	LuaLine = CurrentLocalLine;
	error = luaL_loadfile(LUA, fnaam.c_str()) || lua_pcall(LUA, 0, 0, 0);
	if (error) {
		_lua_showerror();
	}
	LuaLine = -1;
}

bool LuaSetPage(aint n) {
	if (n < 0) {
		Error("sj.set_page: negative page number are not allowed", lp); return false;
	} else if (n > Device->PagesCount - 1) {
		char buf[LINEMAX];
		SPRINTF1(buf, LINEMAX, "sj.set_page: page number must be in range 0..%lu", Device->PagesCount - 1);
		Error(buf, 0, CATCHALL); return false;
	}
	Slot->Page = Device->GetPage(n);
	CheckPage();
	return true;
}

bool LuaSetSlot(aint n) {
	if (n < 0) {
		Error("sj.set_slot: negative slot number are not allowed", lp); return false;
	} else if (n > Device->SlotsCount - 1) {
		char buf[LINEMAX];
		SPRINTF1(buf, LINEMAX, "sj.set_slot: slot number must be in range 0..%lu", Device->SlotsCount - 1);
		Error(buf, 0, CATCHALL); return false;
	}
	Slot = Device->GetSlot(n);
	Device->CurrentSlot = Slot->Number;
	CheckPage();
	return true;
}

namespace lua {

void InsertDirectives() {
	DirectivesTable.Insert_dot("lua", dirLUA, true);
	DirectivesTable.Insert_dot("endlua", dirENDLUA, true);
	DirectivesTable.Insert_dot("includelua", dirINCLUDELUA, true);
}

}
