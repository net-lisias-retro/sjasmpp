/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

#include "syntax.h"

#include "sjerr.h"
#include "parser.h"
#include "sjasm.h"
#include "support.h"
#include "sjio.h"

unsigned long LuaCalculate(char *str) {
	aint val;
	return (long)ParseExpression(str, val) ? val : (long)NULL;
}

void LuaParseLine(char *str) {
	char *ml;

	ml = STRDUP(line);
	if (ml == NULL) 		Error("No enough memory!", 0, FATAL, ERR_RUNTIME);

	STRCPY(line, LINEMAX, str);
	ParseLineSafe();

	STRCPY(line, LINEMAX, ml);
}

void LuaParseCode(char *str) {
	char *ml;

	ml = STRDUP(line);
	if ( NULL == (ml = STRDUP(line)) ) 		Error("No enough memory!", 0, FATAL, ERR_RUNTIME);

	STRCPY(line, LINEMAX, str);
	ParseLineSafe(false);

	STRCPY(line, LINEMAX, ml);
}

