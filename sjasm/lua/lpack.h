/*
* lpack.c
* a Lua library for packing and unpacking binary data
* Luiz Henrique de Figueiredo <lhf@tecgraf.puc-rio.br>
* 31 Jul 2006 13:19:59
* This code is hereby placed in the public domain.
* with contributions from Ignacio Casta�o <castanyo@yahoo.es> and
* Roberto Ierusalimschy <roberto@inf.puc-rio.br>.
*/

#ifndef __LUA_LPACK_H__
#define __LUA_LPACK_H__

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

int luaopen_pack(lua_State *L);

#endif //__LUA_LPACK_H__
