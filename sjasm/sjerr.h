/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef SJASM_SJERR_H_
#define SJASM_SJERR_H_

#include "sjdefs.h"

// EXIT Codes (inspired by Konamiman)
#define ERR_NOERR 0
#define ERR_INTERNAL 1
#define ERR_BAD_CODE 2
#define ERR_FILE_ERROR 3
#define ERR_FILE_OPEN 4
#define ERR_RUNTIME -1
#define ERR_NO_INPUT -2
#define ERR_INVALID_OPTION -3
#define ERR_FILE_IO -4

// TODO: Get rid of this LASTPASS thing and go straight to PASS3
#define ELASTPASS PASS3

enum EStatus { ALL, PASS1, PASS2, PASS3, FATAL, CATCHALL, SUPPRESS };

void Error(char const *, char const *, EStatus const = PASS2, int const = ERR_BAD_CODE);
void Warning(char const *, char const *, EStatus const = PASS2);


#endif /* SJASM_SJERR_H_ */
