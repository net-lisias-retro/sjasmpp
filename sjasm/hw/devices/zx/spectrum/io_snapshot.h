/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a product,
     an acknowledgment in the product documentation would be appreciated but is
     not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

// io_snapshots.h

#ifndef __HW_DEVICES_ZX_SPECTRUM_IO_SNAPSHOTS_H__
#define __HW_DEVICES_ZX_SPECTRUM_IO_SNAPSHOTS_H__

int SaveSNA_ZX(const char* fname, unsigned short start);

#endif //__HW_DEVICES_ZX_SPECTRUM_IO_SNAPSHOTS_H__

//eof io_snapshots.h
