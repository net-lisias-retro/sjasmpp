/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef __HW_DEVICES_ZX_SPECTRUM_HOBETA_FILENAME_H__
#define __HW_DEVICES_ZX_SPECTRUM_HOBETA_FILENAME_H__

#include "filename.h"

class HobetaFilename {
	std::string Content;
	static const char FILLER = ' ';
	static const std::size_t NAME_SIZE = 8;
	static const std::size_t MIN_TYPE_SIZE = 1;
	static const std::size_t MAX_TYPE_SIZE = 3;
public:
	HobetaFilename() : Content(NAME_SIZE + MIN_TYPE_SIZE, FILLER) {}
	HobetaFilename(const HobetaFilename& rh) : Content(rh.Content) {}
	explicit HobetaFilename(const std::string& rh) {
		const std::string::size_type dotPos = rh.find_first_of('.');
		Content = rh.substr(0, dotPos);
		Content.resize(NAME_SIZE, FILLER);
		if (dotPos != std::string::npos) {
			Content += rh.substr(dotPos + 1, MAX_TYPE_SIZE);
		} else {
			Content.append(MIN_TYPE_SIZE, FILLER);
		}
	}

	std::string GetType() const {
		return Content.substr(NAME_SIZE);
	}

	bool Empty() const {
		return Content.empty();
	}

	const void* GetTrDosEntry() const {
		return Content.data();
	}

	std::size_t GetTrdDosEntrySize() const {
		return Content.size();
	}

	const char* c_str() const {
		return Content.c_str();
	}
};

#endif //__HW_DEVICES_ZX_SPECTRUM_DIRECTIVES_HOBETA_FILENAME_H__
