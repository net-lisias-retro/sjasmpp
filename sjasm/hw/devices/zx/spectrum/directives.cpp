/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
     that you wrote the original software. If you use this software in a product,
     an acknowledgment in the product documentation would be appreciated but is
     not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "directives.h"

#include "sjerr.h"
#include "main.h"
#include "sjasm.h"
#include "sjio.h"
#include "reader.h"
#include "parser.h"
#include "lexical.h"
#include "io_tape.h"
#include "io_snapshot.h"
#include "io_trd.h"

extern CFunctionTable DirectivesTable;

static void dirSAVETAP() {
	bool exec = true;

	if (!DeviceID) {
		if (pass == LASTPASS) {
			Error("SAVETAP only allowed in real device emulation mode (See DEVICE)", 0);
		}
		exec = false;
	} else if (pass != LASTPASS) {
		exec = false;
	}

	if (exec && !IsZXSpectrumDevice(DeviceID)) {
		Error("[SAVETAP] Device must be ZXSPECTRUM48, ZXSPECTRUM128, ZXSPECTRUM256, ZXSPECTRUM512 or ZXSPECTRUM1024.", 0);
		exec = false;
	}

	aint val;
	int start = -1;

	const Filename& filename = GetFileName(lp);
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVETAP] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVETAP] Negative values are not allowed", bp, PASS3); return;
			}
			start = val;
		} else {
			Error("[SAVETAP] Syntax error. No parameters", bp, PASS3); return;
		}
	} else if (StartAddress < 0) {
		Error("[SAVETAP] Syntax error. No parameters", bp, PASS3); return;
	} else {
		start = StartAddress;
	}

	if (exec && !SaveTAP_ZX(filename.c_str(), start)) {
		Error("[SAVETAP] Error writing file (Disk full?)", bp, CATCHALL); return;
	}
}

static void dirSAVEBIN() {
	bool exec = true;

	if (!DeviceID) {
		if (pass == LASTPASS) {
			Error("SAVEBIN only allowed in real device emulation mode (See DEVICE)", 0);
		}
		exec = false;
	} else if (pass != LASTPASS) {
		exec = false;
	}

	aint val;
	int start = -1,length = -1;

	const Filename& fnaam = GetFileName(lp);
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVEBIN] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVEBIN] Values less than 0000h are not allowed", bp, PASS3); return;
			} else if (val > 0xFFFF) {
				Error("[SAVEBIN] Values more than FFFFh are not allowed", bp, PASS3); return;
			}
			start = val;
		} else {
			Error("[SAVEBIN] Syntax error. No parameters", bp, PASS3); return;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVEBIN] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVEBIN] Negative values are not allowed", bp, PASS3); return;
			}
			length = val;
		}
	} else {
		Error("[SAVEBIN] Syntax error. No parameters", bp, PASS3); return;
	}

	if (exec && !SaveBinary(fnaam.c_str(), start, length)) {
		Error("[SAVEBIN] Error writing file (Disk full?)", bp, CATCHALL); return;
	}
}

static void dirSAVEHOB() {
	aint val;
	int start = -1,length = -1;
	bool exec = true;

	if (!DeviceID) {
		if (pass == LASTPASS) {
			Error("SAVEHOB only allowed in real device emulation mode (See DEVICE)", 0);
		}
		exec = false;
	} else if (pass != LASTPASS) {
		exec = false;
	}

	const Filename& fnaam = GetFileName(lp);
	HobetaFilename fnaamh;
	if (comma(lp)) {
		if (!comma(lp)) {
			fnaamh = GetHobetaFileName(lp);
		} else {
			Error("[SAVEHOB] Syntax error. No parameters", bp, PASS3); return;
		}
	}
	if (fnaamh.Empty()) {
		Error("[SAVEHOB] Syntax error", bp, PASS3); return;
	}
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVEHOB] Syntax error", bp, PASS3); return;
			}
			if (val < 0x4000) {
				Error("[SAVEHOB] Values less than 4000h are not allowed", bp, PASS3); return;
			} else if (val > 0xFFFF) {
				Error("[SAVEHOB] Values more than FFFFh are not allowed", bp, PASS3); return;
			}
			start = val;
		} else {
			Error("[SAVEHOB] Syntax error. No parameters", bp, PASS3); return;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVEHOB] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVEHOB] Negative values are not allowed", bp, PASS3); return;
			}
			length = val;
		}
	} else {
		Error("[SAVEHOB] Syntax error. No parameters", bp, PASS3); return;
	}
	if (exec && !SaveHobeta(fnaam, fnaamh, start, length)) {
		Error("[SAVEHOB] Error writing file (Disk full?)", bp, CATCHALL); return;
	}
}

static void dirEMPTYTRD() {
	if (pass != LASTPASS) {
		SkipParam(lp);
		return;
	}
	const Filename& fnaam = GetFileName(lp);
	if (fnaam.empty()) {
		Error("[EMPTYTRD] Syntax error", bp, CATCHALL); return;
	}
	TRD_SaveEmpty(fnaam);
}

static void dirSAVETRD() {
	bool exec = true;

	if (!DeviceID) {
		if (pass == LASTPASS) {
			Error("SAVETRD only allowed in real device emulation mode (See DEVICE)", 0);
		}
		exec = false;
	} else if (pass != LASTPASS) {
		exec = false;
	}

	aint val;
	int start = -1,length = -1,autostart = -1; //autostart added by boo_boo 19_0ct_2008

	const Filename& fnaam = GetFileName(lp);
	HobetaFilename fnaamh;
	if (comma(lp)) {
		if (!comma(lp)) {
			fnaamh = GetHobetaFileName(lp);
		} else {
			Error("[SAVETRD] Syntax error. No parameters", bp, PASS3); return;
		}
	}
	if (fnaamh.Empty()) {
		Error("[SAVETRD] Syntax error. Filename should not be empty", bp, PASS3); return;
	}
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVETRD] Syntax error", bp, PASS3); return;
			}
			if (val > 0xFFFF) {
				Error("[SAVETRD] Values more than 0FFFFh are not allowed", bp, PASS3); return;
			}
			start = val;
		} else {
			Error("[SAVETRD] Syntax error. No parameters", bp, PASS3); return;
		}
		if (comma(lp)) {
			if (!comma(lp)) {
				if (!ParseExpression(lp, val)) {
					Error("[SAVETRD] Syntax error", bp, PASS3); return;
				}
				if (val < 0) {
					Error("[SAVETRD] Negative values are not allowed", bp, PASS3); return;
				}
				length = val;
			} else {
				Error("[SAVETRD] Syntax error. No parameters", bp, PASS3); return;
			}
		}
		if (comma(lp)) { //added by boo_boo 19_0ct_2008
			if (!ParseExpression(lp, val)) {
				Error("[SAVETRD] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVETRD] Negative values are not allowed", bp, PASS3); return;
			}
			autostart = val;
		}
	} else {
		Error("[SAVETRD] Syntax error. No parameters", bp, PASS3); return;
	}

	if (exec) {
		TRD_AddFile(fnaam, fnaamh, start, length, autostart);
	}
}

static void dirSAVESNA() {
	bool exec = true;

	if (!DeviceID) {
		if (pass == LASTPASS) {
			Error("SAVESNA only allowed in real device emulation mode (See DEVICE)", 0);
		}
		exec = false;
	} else if (pass != LASTPASS) {
		exec = false;
	}

	if (exec && !IsZXSpectrumDevice(DeviceID)) {
		Error("[SAVESNA] Device must be ZXSPECTRUM48 or ZXSPECTRUM128.", 0);
		exec = false;
	}

	aint val;
	int start = -1;

	const Filename& fnaam = GetFileName(lp);
	if (comma(lp)) {
		if (!comma(lp) && StartAddress < 0) {
			if (!ParseExpression(lp, val)) {
				Error("[SAVESNA] Syntax error", bp, PASS3); return;
			}
			if (val < 0) {
				Error("[SAVESNA] Negative values are not allowed", bp, PASS3); return;
			}
			start = val;
		} else {
			Error("[SAVESNA] Syntax error. No parameters", bp, PASS3); return;
		}
	} else if (StartAddress < 0) {
		Error("[SAVESNA] Syntax error. No parameters", bp, PASS3); return;
	} else {
		start = StartAddress;
	}

	if (exec && !SaveSNA_ZX(fnaam.c_str(), start)) {
		Error("[SAVESNA] Error writing file (Disk full?)", bp, CATCHALL); return;
	}
}

namespace hw { namespace devices { namespace zx { namespace spectrum {

void InsertDirectives() {
	DirectivesTable.Insert_dot("savetap", dirSAVETAP, true);
	DirectivesTable.Insert_dot("savehob", dirSAVEHOB, true);
	DirectivesTable.Insert_dot("savebin", dirSAVEBIN, true);
	DirectivesTable.Insert_dot("emptytrd", dirEMPTYTRD, true);
	DirectivesTable.Insert_dot("savetrd", dirSAVETRD, true);
	DirectivesTable.Insert_dot("savesna", dirSAVESNA, true);
}

}}}}
