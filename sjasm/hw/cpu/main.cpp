/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "main.h"

#include "sjerr.h"
#include "options.h"
#include "z80.h"

namespace hw { namespace cpu {

char const * const NAMES[] = {
		"Z80",
		"Z80-STRICT",
		"Z80-DOCUMENTED",
};

void (*GetOpcode)(void);

void Init() {
	switch (Options::DefaultCpu) {
		case cpuidZ80_DOCUMENTED:
		case cpuidZ80_STRICT:
		case cpuidZ80:
			Select(Options::DefaultCpu);
			break;
		default:
			Error("Default CPU not implemented!", 0, FATAL, ERR_RUNTIME);
	}
}

void Select(hw::cpu::ID cpuid) {
	switch (Options::DefaultCpu) {
		case cpuidZ80_DOCUMENTED:
		case cpuidZ80_STRICT:
		case cpuidZ80:
			hw::cpu::Z80::config::allow_fake_instructions = true;
			hw::cpu::Z80::config::allow_undocumented_instructions = true;
			break;

		default:
			Error("CPU not implemented!", 0, FATAL, ERR_INTERNAL);
	}

	switch (cpuid) {
		case cpuidZ80_DOCUMENTED:
			hw::cpu::Z80::config::allow_undocumented_instructions = false;
			/* No Break */
		case cpuidZ80_STRICT:
			hw::cpu::Z80::config::allow_fake_instructions = false;
			/* No Break */
		case cpuidZ80:
			hw::cpu::Z80::Init();
			GetOpcode = hw::cpu::Z80::GetOpCode;
			break;
	}
}

}}

