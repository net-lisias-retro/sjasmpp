/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/


#ifndef __SJASM_SUPPORT_MINGW_H__
#define __SJASM_SUPPORT_MINGW_H__

#include <windows.h>
#include <sys/time.h>

char *mingw_strdup(const char *str);
#define STRDUP mingw_strdup

// FIXME The lines below probably should be applied too for Linux/MacOS...
#include <cstring>
#define STRCAT(strDestination, sizeInBytes, strSource) std::strcat(strDestination, strSource)
#define STRCPY(strDestination, sizeInBytes, strSource) std::strcpy(strDestination, strSource)
#define STRNCPY(strDestination, sizeInBytes, strSource, count) std::strncpy(strDestination, strSource, count)
#define STRNCAT(strDest, bufferSizeInBytes, strSource, count) std::strncat(strDest, strSource, count)

#define FOPEN(pFile, filename, mode) (pFile = fopen(filename, mode))
#define FOPEN_ISOK(pFile, filename, mode) ((pFile = fopen(filename, mode)) != NULL)

#define SPRINTF1(buffer, sizeOfBuffer, format, arg1) std::sprintf(buffer, format, arg1)
#define SPRINTF2(buffer, sizeOfBuffer, format, arg1, arg2) std::sprintf(buffer, format, arg1, arg2)
#define SPRINTF3(buffer, sizeOfBuffer, format, arg1, arg2, arg3) std::sprintf(buffer, format, arg1, arg2, arg3)

#endif // __SJASM_SUPPORT_MINGW_H__
