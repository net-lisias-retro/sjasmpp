/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

// CAUTION : This file should be included from ~/support.cpp, but should remain compilable stand alone!!

#include "support.h"

// https://msdn.microsoft.com/en-us/library/windows/desktop/aa364934(v=vs.85).aspx
DWORD WINAPI GetCurrentDirectory(DWORD whatever, LPTSTR pad) {
	pad[0] = 0;
	return 0;
}

// https://msdn.microsoft.com/en-us/library/windows/desktop/aa365527(v=vs.85).aspx
DWORD WINAPI SearchPath(LPCTSTR lpPath, LPCTSTR lpFileName, LPCTSTR lpExtension, DWORD nBufferLength, LPTSTR lpBuffer, LPTSTR * lpFilePart) {
	FILE* fp;
	char* p, * f;
	if (lpFileName[0] == '/') {
		STRCPY(lpBuffer, nBufferLength, lpFileName);
	} else {
		STRCPY(lpBuffer, nBufferLength, lpPath);
		if (*lpBuffer && lpBuffer[strlen(lpBuffer)] != '/') {
			STRCAT(lpBuffer, nBufferLength, "/");
		}
		STRCAT(lpBuffer, nBufferLength, lpFileName);
	}
	if (lpFilePart) {
		p = f = lpBuffer;
		while (*p) {
			if (*p == '/') {
				f = p + 1;
			} ++p;
		}
		*lpFilePart = f;
	}
	if (FOPEN_ISOK(fp, lpBuffer, "r")) {
		fclose(fp);
		return true;
	}
	return false;
}

// https://msdn.microsoft.com/en-us/library/windows/desktop/ms724408(v=vs.85).aspx
DWORD WINAPI GetTickCount() {
	struct timeval tv1[1];
	gettimeofday(tv1, 0);
	return tv1->tv_usec / 1000;
}
