/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  Authors:
		Lisias - retro@lisias.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

#ifndef __VERSION_H__
#define __VERSION_H__

extern char const * const PRODUCT_VERSION;
extern char const * const PRODUCT_VERSION_MAJOR;
extern char const * const PRODUCT_VERSION_MINOR;
extern char const * const PRODUCT_VERSION_RELEASE;
extern char const * const PRODUCT_BUILT_AT;
extern char const * const COPYRIGHT_YEAR;

#endif //__VERSION_H__
