/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Konamiman - nestor.soriano@sunhotels.net
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// reader.cpp

#include <algorithm>

#include "reader.h"

#include "sjerr.h"
#include "sjasm.h"
#include "sjio.h"
#include "options.h"
#include "parser.h" // FIXME: Parser should use the Reader, not vice-versa!

#include "modes/compass/lexical.h"

bool cmphstr(char*& p1, char const * p2) {
	unsigned int i = 0;
	/* old:
	if (isupper(*p1))
	  while (p2[i]) {
		if (p1[i]!=toupper(p2[i])) return 0;
		++i;
	  }
	else
	  while (p2[i]) {
		if (p1[i]!=p2[i]) return 0;
		++i;
	  }*/
	/* (begin) */
	unsigned int v = 0;
	if (strlen(p1) >= strlen(p2)) {
		if (isupper(*p1)) {
			while (p2[i]) {
				if (p1[i] != toupper(p2[i])) {
					v = 0;
				} else {
					++v;
				}
				++i;
			}
			if (strlen(p2) != v) {
				return false;
			}
		} else {
			while (p2[i]) {
				if (p1[i] != p2[i]) {
					v = 0;
				} else {
					++v;
				}
				++i;
			}
			if (strlen(p2) != v) {
				return false;
			}
		}
		/* (end) */

		if (i <= strlen(p1) && p1[i] > ' '/* && p1[i]!=':'*/) {
			return false;
		}
		p1 += i;
		return true;
	} else {
		return false;
	}
}

void SkipBlanks(char*& p) {
  while (*p && *p > 0 && *p <= ' ') {
    ++p;
  }
}

bool SkipBlanks() {
	SkipBlanks(lp);
	return (*lp == 0);
}

void SkipParam(char*& p) {
	SkipBlanks(p);
	if (!(*p)) {
		return;
	}
	while (((*p) != '\0') && ((*p) != ',')) {
		p++;
	}
}

bool NeedEQU() {
	char* olp = lp;
	SkipBlanks();
	/*if (*lp=='=') { ++lp; return 1; }*/
	/* cut: if (*lp=='=') { ++lp; return 1; } */
	if (*lp == '.') {
		++lp;
	}
	if (cmphstr(lp, "equ")) {
		return true;
	}
	lp = olp;
	return false;
}

bool NeedDEFL() {
	char* olp = lp;
	SkipBlanks();
	if (*lp == '=') {
		++lp;
		return true;
	}
	if (*lp == '.') {
		++lp;
	}
	if (cmphstr(lp, "defl")) {
		return true;
	}
	lp = olp;
	return false;
}

bool NeedField() {
	char* olp = lp;
	SkipBlanks();
	if (*lp == '#') {
		++lp; return true;
	}
	if (*lp == '.') {
		++lp;
	}
	if (cmphstr(lp, "field")) {
		return true;
	}
	lp = olp;
	return false;
}

bool comma(char*& p) {
	SkipBlanks(p);
	if (*p != ',') {
		return false;
	}
	++p; return true;
}

int cpc = '4';

bool oparen(char*& p, char c) {
	SkipBlanks(p);
	if (*p != c) {
		return false;
	}
	if (c == '[') {
		cpc = ']';
	}
	if (c == '(') {
		cpc = ')';
	}
	if (c == '{') {
		cpc = '}';
	}
	++p;
	return true;
}

bool cparen(char*& p) {
	SkipBlanks(p);
	if (*p != cpc) {
		return false;
	}
	++p;
	return true;
}

char* getparen(char* p) {
	int teller = 0;
	SkipBlanks(p);
	while (*p) {
		if (*p == '(') {
			++teller;
		} else if (*p == ')') {
			if (teller == 1) {
				SkipBlanks(++p); return p;
			} else {
				--teller;
			}
		}
		++p;
	}
	return 0;
}

char nidtemp[LINEMAX];
char* GetID(char*& p) {
	/*char nid[LINEMAX],*/ char* np;
	np = nidtemp;
	SkipBlanks(p);
	//if (!isalpha(*p) && *p!='_') return 0;
	if (*p && !isalpha((unsigned char) * p) && *p != '_') {
		return 0;
	}
	while (*p) {
		if (IsNotValidIdChar(*p)) break;
		*np = *p; ++p; ++np;
	}
	*np = 0;
	/*return STRDUP(nid);*/
	return nidtemp;
}

char* getinstr(char*& p) {
	static char instrtemp[LINEMAX];
	/*char nid[LINEMAX],*/ char* np = instrtemp;
	SkipBlanks(p);
	if (!isalpha((unsigned char) * p) && *p != '.') {
		return 0;
	} else {
		*np = *p; ++p; ++np;
	}
	while (*p) {
		if (!isalnum((unsigned char) * p) && *p != '_') {
			break;
		} /////////////////////////////////////
		*np = *p; ++p; ++np;
	}
	*np = 0;
	/*return STRDUP(nid);*/
	return instrtemp;
}

/* changes applied from SjASM 0.39g */
bool check8(aint val, bool error) {
	// TODO: Táqui o erro estranho de "Bytes Lost". Checar com calma!
	if (val != (val & 255) && ~val > 127 && error) {
		Error("Bytes lost", 0); return false;
	}
	return true;
}

/* changes applied from SjASM 0.39g */
bool check8o(long val) {
	if (val < -128 || val > 127) {
		Error("Offset out of range", 0); return false;
	}
	return true;
}

/* changes applied from SjASM 0.39g */
bool check16(aint val, bool error) {
	// TODO: Táqui o erro estranho de "Bytes Lost". Checar com calma!
	if (val != (val & 65535) && ~val > 32767 && error) {
		Error("Bytes lost", 0); return false;
	}
	return true;
}

/* changes applied from SjASM 0.39g */
bool check24(aint val, bool error) {
	// TODO: Táqui o erro estranho de "Bytes Lost". Checar com calma!
	if (val != (val & 16777215) && ~val > 8388607 && error) {
		Error("Bytes lost", 0); return false;
	}
	return true;
}

bool need(char*& p, char c) {
	SkipBlanks(p);
	if (*p != c) {
		return false;
	}
	++p; return true;
}

int needa(char*& p, char const * c1, int r1, char const * c2, int r2, char const * c3, int r3) {
	//  SkipBlanks(p);
	if (!isalpha((unsigned char) * p)) {
		return 0;
	}
	if (cmphstr(p, c1)) {
		return r1;
	}
	if (c2 && cmphstr(p, c2)) {
		return r2;
	}
	if (c3 && cmphstr(p, c3)) {
		return r3;
	}
	return 0;
}

int need(char*& p, const char *c) {
	SkipBlanks(p);
	while (*c) {
		if (*p != *c) {
			c += 2; continue;
		}
		++c;
		if (*c == ' ') {
			++p; return *(c - 1);
		}
		if (*c == '_' && *(p + 1) != *(c - 1)) {
			++p; return *(c - 1);
		}
		if (*(p + 1) == *c) {
			p += 2; return *(c - 1) + *c;
		}
		++c;
	}
	return 0;
}

int getval(int p) {
	switch (p) {
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		return p - '0';
	default:
		if (isupper((unsigned char)p)) {
			return p - 'A' + 10;
		}
		if (islower((unsigned char)p)) {
			return p - 'a' + 10;
		}
		return 200;
	}
}

bool GetConstant(char*& op, aint& val) {
	aint base,pb = 1,v,oval;
	char* p = op,* p2,* p3;

	SkipBlanks(p);

	p3 = p;
	val = 0;

	// Ugly hack to allow &B and &H style constants! (as did by Konamiman)
	if (*p == '&') {
		p++;
		char prefix = tolower(*p);
		if (prefix != 'h' && prefix != 'b') {
			Error("Syntax error", op, CATCHALL);
			return false;
		}
		*p = ('h' == prefix ) ? '#' : '%';
	}

	switch (*p) {
		case '#':
		case '$': {
			++p;
			bool isValidSpace;
			while (isalnum((unsigned char) *p) || (isValidSpace = isValidSpaceInsideConstant(*p))) {
				if (isValidSpace) {
					p++;
					continue;
				}

				if ((v = getval(*p)) >= 16) {
					Error("Digit not in base", op);
					return false;
				}
				oval = val;
				val = val * 16 + v;
				++p;
				if (oval > val) {
					Error("Overflow", 0, SUPPRESS);
				}
			}

			if (p - p3 < 2) {
				Error("Syntax error", op, CATCHALL);
				return false;
			}

			op = p;

			return true;
		}
		/* no break */
		case '%': {
			++p;
			bool isValidSpace;
			while (isdigit((unsigned char) *p) || (isValidSpace = isValidSpaceInsideConstant(*p))) {
				if (isValidSpace) {
					p++;
					continue;
				}
				if ((v = getval(*p)) >= 2) {
					Error("Digit not in base", op);
					return false;
				}
				oval = val;
				val = val * 2 + v;
				++p;
				if (oval > val) {
					Error("Overflow", 0, SUPPRESS);
				}
			}
			if (p - p3 < 2) {
				Error("Syntax error", op, CATCHALL);
				return false;
			}

			op = p;

			return true;
		}
		/* no break */
		case '0':
			++p;
			if (*p == 'x' || *p == 'X') {
				++p;
				bool isValidSpace;
				while (isalnum((unsigned char) *p) || (isValidSpace = isValidSpaceInsideConstant(*p))) {
					if (isValidSpace) {
						p++;
						continue;
					}
					if ((v = getval(*p)) >= 16) {
						Error("Digit not in base", op);
						return false;
					}
					oval = val;
					val = val * 16 + v;
					++p;
					if (oval > val) {
						Error("Overflow", 0, SUPPRESS);
					}
				}
				if (p - p3 < 3) {
					Error("Syntax error", op, CATCHALL);
					return false;
				}

				op = p;

				return true;
			}
			/* no break */
		default:
			while (isalnum((unsigned char) *p) || isValidSpaceInsideConstant(*p)) {
				++p;
			}
			p2 = p--;
			while (isValidSpaceInsideConstant(*p))
				p--;

			if (isdigit((unsigned char) *p)) {
				base = 10;
			} else
				switch (*p) {
					case 'b':
					case 'B':
						base = 2;
						--p;
						break;
					case 'h':
					case 'H':
						base = 16;
						--p;
						break;
					case 'o':
					case 'O':
					case 'q':
					case 'Q':
						base = 8;
						--p;
						break;
					case 'd':
					case 'D':
						base = 10;
						--p;
						break;
					default:
						return false;
				}

			do {
				if (isValidSpaceInsideConstant(*p)) continue;
				if ((v = getval(*p)) >= base) {
					Error("Digit not in base", op);
					return false;
				}
				oval = val;
				val += v * pb;
				if (oval > val) Error("Overflow", 0, SUPPRESS);
				pb *= base;
			} while (p-- != p3);

			op = p2;

			return true;
	}
}

bool GetCharConstChar(char*& op, aint& val) {
	if ((val = *op++) != '\\') {
		return true;
	}
	switch (val = *op++) {
		case '\\':
		case '\'':
		case '\"':
		case '\?':
			return true;
		case 'n':
		case 'N':
			val = 10;
			return true;
		case 't':
		case 'T':
			val = 9;
			return true;
		case 'v':
		case 'V':
			val = 11;
			return true;
		case 'b':
		case 'B':
			val = 8;
			return true;
		case 'r':
		case 'R':
			val = 13;
			return true;
		case 'f':
		case 'F':
			val = 12;
			return true;
		case 'a':
		case 'A':
			val = 7;
			return true;
		case 'e':
		case 'E':
			val = 27;
			return true;
		case 'd':
		case 'D':
			val = 127;
			return true;
		default:
			--op;
			val = '\\';
			Error("Unknown escape", op);
			return true;
	}
	return false;
}

bool GetCharConstCharSingle(char*& op, aint& val) {
	// XXX What the heck? This thing always return true??
	if ((val = *op++) != '\\') {
		return true;
	}
	switch (val = *op++) {
		case '\'':
			return true;
	}
	--op;
	val = '\\';
	return true;
}

bool GetCharConst(char*& p, aint& val) {
	aint s = 24,r,t = 0; val = 0;
	char* op = p,q;
	if (*p != '\'' && *p != '"') {
		return false;
	}
	if (Options::IsCompassCompatibilityMode && ((p[0] == '"' && p[1] == '"') || (p[0] == '\'' && p[1] == '\''))) {
		val = 0;
		p += 2;
		return true;
	}
	q = *p++;
	do {
		if (!*p || *p == q) {
			p = op; return false;
		}
		GetCharConstChar(p, r);
		val += r << s; s -= 8; ++t;
	} while (*p != q);
	if (t > 4) {
		Error("Overflow", 0, SUPPRESS);
	}
	val = val >> (s + 8);
	++p;
	return true;
}

int GetBytes(char*& p, int e[], int add, int dc) {
	aint val;
	int t = 0;
	while (true) {
		SkipBlanks(p);
		if (!*p) {
			Error("Expression expected", 0, SUPPRESS); break;
		}
		if (t == 128) {
			Error("Too many arguments", p, SUPPRESS); break;
		}
		if (*p == '"') {
			p++;
			do {
				if (!*p || *p == '"') {
					Error("Syntax error", p, SUPPRESS); e[t] = -1; return t;
				}
				if (t == 128) {
					Error("Too many arguments", p, SUPPRESS); e[t] = -1; return t;
				}
				GetCharConstChar(p, val); check8(val); e[t++] = (val + add) & 255;
			} while (*p != '"');
			++p; if (dc && t) {
				 	e[t - 1] |= 128;
				 }
		} else if ((*p == 0x27) && (!*(p+2) || *(p+2) != 0x27)) {
		  	p++;
			do {
				if (!*p || *p == 0x27) {
					Error("Syntax error", p, SUPPRESS);
					e[t] = -1;
					return t;
				}
				if (t == 128) {
		  			Error("Too many arguments", p, SUPPRESS);
					e[t] = -1;
					return t;
				}
		  		GetCharConstCharSingle(p, val);
				check8(val);
				e[t++] = (val + add) & 255;
			} while (*p != 0x27);

		  	++p;

			if (dc && t) {
				e[t - 1] |= 128;
			}
		} else {
			if (ParseExpression(p, val)) {
				check8(val); e[t++] = (val + add) & 255;
			} else {
				Error("Syntax error", p, SUPPRESS);
				break;
			}
		}
		SkipBlanks(p);
		if (*p != ',') {
			break;
		}
		++p;
	}
	e[t] = -1;
	return t;
}

std::string GetString(char*& p) {
	SkipBlanks(p);
	if (!*p) return std::string();

	char limiter = '\0';
	if ('"' == *p) {
		limiter = '"';
		++p;
	}

	//TODO: research strange ':' logic
	std::string result;
	while (*p && *p != limiter) {
		result += *p++;
	}
	if (*p != limiter)	Error((std::string("No closing '") + limiter + "'").c_str(), 0);
	if (*p)				++p;
	return result;
}

Filename GetFileName(char*& p) {
	const std::string& result = GetString(p);
	return Filename(result);
}

Filename GetFileName(char*& p, bool& search_local_first) {
	SkipBlanks(p);
	if (!*p) return Filename(std::string());

	char limiter = '\0';
	if ('"' == *p) {
		limiter = '"';
		++p;
	} else if ('<' == *p) {
		limiter = '>';
		++p;
	}

	//TODO: research strange ':' logic
	std::string result;
	while (*p && *p != limiter) {
		result += *p++;
	}
	if (*p != limiter)	Error((std::string("No closing '") + limiter + "'").c_str(), 0);
	if (*p)				++p;
	search_local_first = '>' != limiter;
	return Filename(result);
}

bool needcomma(char*& p) {
	SkipBlanks(p);
	if (*p != ',') {
		Error("Comma expected", 0);
	}
	return (*(p++) == ',');
}

bool needbparen(char*& p) {
	SkipBlanks(p);
	if (*p != ']') {
		Error("']' expected", 0);
	}
	return (*(p++) == ']');
}

bool islabchar(char p) {
	// FIXME Change this to a macro!
	return (isalnum((unsigned char)p) || p == '_' || p == '.' || p == '?' || p == '!' || p == '#' || p == '@');
}

EStructureMembers GetStructMemberId(char*& p) {
	if (*p == '#') {
		++p; if (*p == '#') {
			 	++p; return SMEMBALIGN;
			 } return SMEMBBLOCK;
	}
	//  if (*p=='.') ++p;
	switch (*p * 2 + *(p + 1)) {
		case 'b' * 2 + 'y':
		case 'B' * 2 + 'Y':
			if (cmphstr(p, "byte")) {
				return SMEMBBYTE;
			}
			break;
		case 'w' * 2 + 'o':
		case 'W' * 2 + 'O':
			if (cmphstr(p, "word")) {
				return SMEMBWORD;
			}
			break;
		case 'b' * 2 + 'l':
		case 'B' * 2 + 'L':
			if (cmphstr(p, "block")) {
				return SMEMBBLOCK;
			}
			break;
		case 'd' * 2 + 'b':
		case 'D' * 2 + 'B':
			if (cmphstr(p, "db")) {
				return SMEMBBYTE;
			}
			break;
		case 'd' * 2 + 'w':
		case 'D' * 2 + 'W':
			if (cmphstr(p, "dw")) {
				return SMEMBWORD;
			}
			if (cmphstr(p, "dword")) {
				return SMEMBDWORD;
			}
			break;
		case 'd' * 2 + 's':
		case 'D' * 2 + 'S':
			if (cmphstr(p, "ds")) {
				return SMEMBBLOCK;
			}
			break;
		case 'd' * 2 + 'd':
		case 'D' * 2 + 'D':
			if (cmphstr(p, "dd")) {
				return SMEMBDWORD;
			}
			break;
		case 'a' * 2 + 'l':
		case 'A' * 2 + 'L':
			if (cmphstr(p, "align")) {
				return SMEMBALIGN;
			}
			break;
		case 'd' * 2 + 'e':
		case 'D' * 2 + 'E':
			if (cmphstr(p, "defs")) {
				return SMEMBBLOCK;
			}
			if (cmphstr(p, "defb")) {
				return SMEMBBYTE;
			}
			if (cmphstr(p, "defw")) {
				return SMEMBWORD;
			}
			if (cmphstr(p, "defd")) {
				return SMEMBDWORD;
			}
			break;
		case 'd' * 2 + '2':
		case 'D' * 2 + '2':
			if (cmphstr(p, "d24")) {
				return SMEMBD24;
			}
			break;
		default:
			break;
	}
	return SMEMBUNKNOWN;
}

int GetArray(char*& p, int e[], int add, int dc) {
	aint val;
	int t = 0;
	while (true) {
		SkipBlanks(p);
		if (!*p) {
			Error("Expression expected", 0, SUPPRESS); break;
		}
		if (t == 128) {
			Error("Too many arguments", p, SUPPRESS); break;
		}
		if (*p == '"') {
			p++;
			do {
				if (!*p || *p == '"') {
					Error("Syntax error", p, SUPPRESS); e[t] = -1; return t;
				}
				if (t == 128) {
					Error("Too many arguments", p, SUPPRESS); e[t] = -1; return t;
				}
				GetCharConstChar(p, val); check8(val); e[t++] = (val + add) & 255;
			} while (*p != '"');
			++p; if (dc && t) {
				 	e[t - 1] |= 128;
				 }
		} else if ((*p == 0x27) && (!*(p+2) || *(p+2) != 0x27)) {
		  	p++;
			do {
				if (!*p || *p == 0x27) {
					Error("Syntax error", p, SUPPRESS); e[t] = -1; return t;
				}
				if (t == 128) {
		  			Error("Too many arguments", p, SUPPRESS); e[t] = -1; return t;
				}
		  		GetCharConstCharSingle(p, val); check8(val); e[t++] = (val + add) & 255;
			} while (*p != 0x27);
		  	++p;
			if (dc && t) {
				 e[t - 1] |= 128;
			}
		} else {
			if (ParseExpression(p, val)) {
				check8(val); e[t++] = (val + add) & 255;
			} else {
				Error("Syntax error", p, SUPPRESS); break;
			}
		}
		SkipBlanks(p); if (*p != ',') {
					   	break;
					   } ++p;
	}
	e[t] = -1; return t;
}

//eof reader.cpp
