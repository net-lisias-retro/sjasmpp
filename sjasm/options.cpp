/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Vitamin/CAIG - vitamin.caig@gmail.com
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// options.cpp

#include <string.h>
#include <list>

#include "options.h"

#include "sjdefs.h"
#include "sjasm.h"
#include "filename.h"

namespace {
	const char HELP[] = "help";
	const char LSTLAB[] = "lstlab";
	const char SYM[] = "sym";
	const char LST[] = "lst";
	const char EXP[] = "exp";
	const char RAW[] = "raw";
	const char FULLPATH[] = "fullpath";
	const char REVERSEPOP[] = "reversepop";
	const char NOLOGO[] = "nologo";
	const char DOS866[] = "dos866";
	const char DIRBOL[] = "dirbol";
	const char INC[] = "inc";
	const char COMPASS_MODE[] = "compass-mode";
	const char SJASMPLUS_MODE[] = "sjasmplus-mode";
	const char DEFAULT_CPU[] = "default-cpu";
}

namespace Options {
	Filename SymbolListFName;
	Filename ListingFName;
	Filename ExportFName;
	Filename DestionationFName;
	Filename RAWFName;
	Filename UnrealLabelListFName;

	bool IsPseudoOpBOF = false;
	bool IsReversePOP = false;
	bool IsShowFullPath = false;
	bool AddLabelListing = false;
	bool HideLogo = false;
	bool NoDestinationFile = true; // not *.out files by default
	bool IsHelp = false;
	bool IsSjasmCompatibilityMode = true;
	bool IsSjasmPlusCompatibilityMode = false;
	bool IsCompassCompatibilityMode = false;
	hw::cpu::ID DefaultCpu = hw::cpu::ID::cpuidZ80;

	std::list<std::string> IncludeDirsList;

	bool GetOptions(const char* argv[], int& i) {
		while (argv[i] && *argv[i] == '-') {
			//TODO: do not support single-dashed options
			const std::string option(argv[i][1] == '-' ? argv[i++] + 2 : argv[i++] + 1);
			const std::string::size_type eqPos = option.find("=");
			const std::string& optName = option.substr(0, eqPos);
			const std::string& optValue = eqPos != std::string::npos ? option.substr(eqPos + 1) : std::string();

			if (optName == HELP) {
				IsHelp = true;
			} else if (optName == LSTLAB) {
				AddLabelListing = true;
			} else if (optName == FULLPATH) {
				IsShowFullPath = true;
			} else if (optName == REVERSEPOP) {
				IsReversePOP = true;
			} else if (optName == NOLOGO) {
				HideLogo = true;
			} else if (optName == DOS866) {
				ConvertEncoding = ENCDOS;
			} else if (optName == DIRBOL) {
				IsPseudoOpBOF = true;
			} else if (optName == SYM) {
				if (!optValue.empty()) {
					SymbolListFName = Filename(optValue);
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == LST) {
				if (!optValue.empty()) {
					ListingFName = Filename(optValue);
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == EXP) {
				if (!optValue.empty()) {
					ExportFName = Filename(optValue);
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == RAW) {
				if (!optValue.empty()) {
					RAWFName = Filename(optValue);
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == INC) {
				if (!optValue.empty()) {
					IncludeDirsList.push_front(optValue);
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == DEFAULT_CPU) {
				if (!optValue.empty()) {
					int i;
					for ( i = 0; i < hw::cpu::cpuidENDMARK; i++) {
						if (optValue == hw::cpu::NAMES[i]) {
							DefaultCpu = (hw::cpu::ID)i;
							break;
						}
					}
					if (i == hw::cpu::cpuidENDMARK) {
						_CERR "Invalid value in " _CMDL argv[i-1] _ENDL;
						return false;
					}
				} else {
					_CERR "No parameters found in " _CMDL argv[i-1] _ENDL;
					return false;
				}
			} else if (optName == SJASMPLUS_MODE) {
				IsSjasmPlusCompatibilityMode = true;
			} else if (optName == COMPASS_MODE) {
				IsCompassCompatibilityMode = true;
			} else if (option.size() > 1 && (option[0] == 'i' || option[0] == 'I')) {
				IncludeDirsList.push_front(option.substr(1));
			} else {
				_CERR "Unrecognized option: " _CMDL option _ENDL;
				return false;
			}
		}
		return true;
	}

	void ShowHelp() {
		_COUT "\nOption flags as follows:" _ENDL;
		_COUT "  --" _CMDL HELP _CMDL "                   Help information (you see it)" _ENDL;
		_COUT "  -i<path> or -I<path> or --" _CMDL INC _CMDL "=<path>" _ENDL;
		_COUT "                           Include path" _ENDL;
		_COUT "  --" _CMDL LST _CMDL "=<filename>         Save listing to <filename>" _ENDL;
		_COUT "  --" _CMDL LSTLAB _CMDL "                 Enable label table in listing" _ENDL;
		_COUT "  --" _CMDL SYM _CMDL "=<filename>         Save symbols list to <filename>" _ENDL;
		_COUT "  --" _CMDL EXP _CMDL "=<filename>         Save exports to <filename> (see EXPORT pseudo-op)" _ENDL;
		_COUT "  --" _CMDL RAW _CMDL "=<filename>         Save all output to <filename> ignoring OUTPUT pseudo-ops" _ENDL;
		_COUT "  --" _CMDL DEFAULT_CPU _CMDL "=<NAME>     Select the default CPU" _ENDL;
		_COUT " Logging:" _ENDL;
		_COUT "  --" _CMDL NOLOGO _CMDL "                 Do not show startup message" _ENDL;
		_COUT "  --" _CMDL FULLPATH _CMDL "               Show full path to error file" _ENDL;
		_COUT " Other:" _ENDL;
		_COUT "  --" _CMDL REVERSEPOP _CMDL "             Enable reverse POP order (as in base SjASM version)" _ENDL;
		_COUT "  --" _CMDL SJASMPLUS_MODE _CMDL "         Enable SjASMPlus compatibility mode" _ENDL;
		_COUT "  --" _CMDL COMPASS_MODE _CMDL "           Enable COMPASS compatibility mode (as in Konamiman's SjASM version)" _ENDL;
		_COUT "  --" _CMDL DIRBOL _CMDL "                 Enable processing directives from the beginning of line" _ENDL;
		_COUT "  --" _CMDL DOS866 _CMDL "                 Encode from Windows codepage to DOS 866 (Cyrillic)" _ENDL;
	}
} // eof namespace Options
