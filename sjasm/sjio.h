/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// sjio.h
#ifndef __SJIO
#define __SJIO

#include "support.h"
#include "tables.h"
#include "sjasm.h"

enum EReturn { END, ELSE, ENDIF, ENDTEXTAREA, ENDM };

#define PAGESIZE 0x4000 /* added */

extern aint PreviousAddress, epadres;
extern FILE* FP_ListingFile;

#define OUTPUT_TRUNCATE 0
#define OUTPUT_REWIND 1
#define OUTPUT_APPEND 2

void OpenDest(int); /* added from new SjASM 0.39g */
void NewDest(char const * newfilename, int mode); /* added from new SjASM 0.39g */
bool FileExists(char const * filename); /* added from new SjASM 0.39g */
void ListFile();
void ListFileSkip(char*);
void CheckPage(); /* added */
void EmitByte(int byte);
void EmitWord(int word);
void EmitBytes(int* bytes);
void EmitWords(int* words);
void EmitBlock(aint byte, aint len, bool nulled = false);
void OpenFile(char const * nfilename, bool const = true);
void IncludeFile(char const * nfilename, bool const);
void Close();
void OpenList();
void ReadBufLine(bool Parse = true, bool SplitByColon = true); /* added */
void OpenDest();
void PrintHEX32(char*& p, aint h);
void PrintHEX16(char*& p, aint h); /* added */
void PrintHEXAlt(char*& p, aint h); /* added */
char* GetPath(char const * fname, bool const = true);
char* GetPath(char const * fname, char ** filenamebegin, bool const = true);
void BinIncFile(char const * fname, int offset, int length);
bool SaveRAM(FILE*, int, int);
void* SaveRAM(void* dst, int start, int size);
unsigned char MemGetByte(unsigned int address); /* added */
unsigned int MemGetWord(unsigned int address); /* added */
bool SaveBinary(char const * fname, int start, int length); /* added */
bool ReadLine(bool SplitByColon = true);
EReturn ReadFile();
EReturn ReadFile(char const * pp, char const * err); /* added */
EReturn SkipFile();
EReturn SkipFile(char const* pp, const char *err); /* added */
void NewDest(char* newfilename);
void SeekDest(long, int); /* added from new SjASM 0.39g */
bool ReadFileToCStringsList(CStringsList*& f, char const * end);
void WriteExp(char* n, aint v);

#endif
//eof sjio.h

