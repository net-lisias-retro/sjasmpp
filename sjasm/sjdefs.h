/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

//sjdefs.h

#ifndef __SJDEFS
#define __SJDEFS

#define MAXPASSES 4
#define LASTPASS (MAXPASSES-1)

// To make the !#@$#@$#@ Eclipse happy
#ifndef PATH_MAX
#define PATH_MAX 128
#endif

// FIXME This project is now C++ only. Hunt and kill all ANSI C functions.
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using std::flush;

#include <stack>
using std::stack;

// output macros (it was used to support WinCE)
#define _COUT cout <<
#define _CERR cerr <<
#define _CMDL <<
#define _ENDL << endl
#define _END ;

// standard libraries
#ifdef WIN32
#include <windows.h>
#endif


// global defines
#define LINEMAX 2048
#define LINEMAX2 LINEMAX*2
#ifdef DOS
#define LABMAX 32
#define LABTABSIZE 16384
#define FUNTABSIZE 2048
#else
#define LABMAX 64
#define LABTABSIZE 32768
#define FUNTABSIZE 4096
#endif

// XXX Aprisobal changed this to unsigned long, what lead to some warnings on GCC (and also some spurious error detecting on the code!).
// Check all the code for possible drawbacks by setting this back to SIGNED long.
typedef long aint;

#endif
//eof sjdefs.h
