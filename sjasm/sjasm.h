/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

#ifndef __SJASM
#define __SJASM

#include "sjdefs.h"
#include "tables.h"
#include "modules.h"

extern CDevice *Devices;
extern CDevice *Device;
extern CDeviceSlot *Slot;
extern CDevicePage *Page;
extern char* DeviceID;

// extend
extern char filename[LINEMAX], * lp, line[LINEMAX], temp[LINEMAX], pline[LINEMAX2], ErrorLine[LINEMAX2], * bp;
extern char sline[LINEMAX2], sline2[LINEMAX2];

extern char SourceFNames[128][MAX_PATH];
extern int CurrentSourceFName;

extern int ConvertEncoding; /* added */
extern int pass, IsLabelNotFound, ErrorCount, WarningCount, IncludeLevel, IsRunning, donotlist, listmacro;
//physical address, disp != org mode flag
extern int adrdisp, PseudoORG; /* added for spectrum mode */
extern char* MemoryPointer; /* added for spectrum ram */
extern int StartAddress;
extern int macronummer, reglenwidth;
extern bool synerr, lijst;
//$, ...
extern aint CurAddress, AddressOfMAP, CurrentGlobalLine, CurrentLocalLine, CompiledCurrentLine, destlen, size, PreviousErrorLine, maxlin, comlin;

extern void (*GetCPUInstruction)(void);
extern char* vorlabp, * macrolabp, * LastParsedLabel;

enum EEncoding { ENCDOS, ENCWIN };
extern char* CurrentDirectory;

void ExitASM(int p);
extern CStringsList* lijstp;
extern stack< SRepeatStack> RepeatStack;

extern CLabelTable LabelTable;
extern CLocalLabelTable LocalLabelTable;
extern CDefineTable DefineTable;
extern CMacroDefineTable MacroDefineTable;
extern CMacroTable MacroTable;
extern CStructureTable StructureTable;
extern CAddressList* AddressList; /*from SjASM 0.39g*/

extern ModulesList Modules;

#endif
//eof sjasm.h
