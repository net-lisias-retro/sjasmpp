/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Vitamin/CAIG - vitamin.caig@gmail.com
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

#include "defines.h"

#include "sjdefs.h"
#include "sjerr.h"
#include "sjio.h"
#include "tables.h"

// defines.cpp

void CDefineTable::Init() {
	for (int i = 0; i < 128; defs[i++] = 0) {
		;
	}
}

void CDefineTable::Add(char const * name, char const * value, CStringsList * nss/*added*/) {
	if (FindDuplicate(name)) {
		Error("Duplicate define", name);
	}
	defs[*name] = new CDefineTableEntry(name, value, nss, defs[*name]);
}

char* CDefineTable::Get(char* name) {
	CDefineTableEntry* p = defs[*name];
	DefArrayList = 0;
	while (p) {
		if (!strcmp(name, p->name)) {
			if (p->nss) {
				DefArrayList = p->nss;
			}
			return p->value;
		}
		p = p->next;
	}
	return NULL;
}

bool CDefineTable::FindDuplicate(char const * name) {
	CDefineTableEntry* p = defs[*name];
	while (p) {
		if (!strcmp(name, p->name)) {
			return true;
		}
		p = p->next;
	}
	return false;
}

bool CDefineTable::Replace(char const * name, char const * value) {
	CDefineTableEntry* p = defs[*name];
	while (p) {
		if (!strcmp(name, p->name)) {
			delete[](p->value);
			p->value = new char[strlen(value)+1];
			strcpy(p->value,value);

			return false;
		}
		p = p->next;
	}
	defs[*name] = new CDefineTableEntry(name, value, 0, defs[*name]);
	return true;
}

bool CDefineTable::Remove(char* name) {
	CDefineTableEntry* p = defs[*name];
	CDefineTableEntry* p2 = NULL;
	while (p) {
		if (!strcmp(name, p->name)) {
			if (p2 != NULL) {
				p2->next = p->next;
			} else {
				p = p->next;
			}

			return true;
		}
		p2 = p;
		p = p->next;
	}
	return false;
}

void CDefineTable::RemoveAll() {
	for (int i=0; i < 128; i++)
	{
		if (defs[i] != NULL)
		{
			delete defs[i];
			defs[i] = NULL;
		}
	}
}

//eof defines.cpp
