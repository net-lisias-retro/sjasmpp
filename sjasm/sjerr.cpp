/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "sjerr.h"
#include "sjasm.h"
#include "support.h"
#include "sjio.h"

static aint IsSkipErrors = 0;

void Error(char const * fout, char const * bd, EStatus const type, int const exitCode) {
	char * const ep = ErrorLine;

	if (IsSkipErrors && PreviousErrorLine == CurrentLocalLine && type != FATAL) {
		return;
	}
	if (type == CATCHALL && PreviousErrorLine == CurrentLocalLine) {
		return;
	}
	if (type == PASS1 && pass != 1) {
		return;
	}
	if ((type == CATCHALL || type == PASS3) && pass < 3) {
		return;
	}
	if ((type == SUPPRESS || type == PASS2) && pass < 2) {
		return;
	}
	IsSkipErrors = (type == SUPPRESS);
	PreviousErrorLine = CurrentLocalLine;

	++ErrorCount;
	{
		char count[25] = "";
		SPRINTF1(count, 25, "%i", ErrorCount);
		DefineTable.Replace("_ERRORS", count);
	}

	/*SPRINTF3(ep, LINEMAX2, "%s line %lu: %s", filename, CurrentLocalLine, fout);
	if (bd) {
		STRCAT(ep, LINEMAX2, ": "); STRCAT(ep, LINEMAX2, bd);
	}
	if (!strchr(ep, '\n')) {
		STRCAT(ep, LINEMAX2, "\n");
	}*/

	if (pass > LASTPASS) {
		SPRINTF1(ep, LINEMAX2, "error: %s", fout);
	} else {
		SPRINTF3(ep, LINEMAX2, "%s(%lu):: error: %s", filename, CurrentLocalLine, fout);
	}

	if (bd) {
		STRCAT(ep, LINEMAX2, ": ");
		STRCAT(ep, LINEMAX2, bd);
	}
	if (!strchr(ep, '\n')) 		STRCAT(ep, LINEMAX2, "\n");
	if (FP_ListingFile != NULL)	fputs(ErrorLine, FP_ListingFile);

	_CERR ErrorLine _END;

	if (type == FATAL) 	ExitASM(exitCode);
}

void Warning(char const * fout, char const * bd, EStatus type) {
	char* ep = ErrorLine;

	if (type == PASS1 && pass != 1) {
		// FIXME: What the Hell? This is a fatal error, it should not be silenced!
		return;
	}
	if (type == PASS2 && pass < 2) {
		// FIXME: What the Hell? This is a fatal error, it should not be silenced!
		return;
	}

	++WarningCount;
	{
		char count[25] = "";
		SPRINTF1(count, 25, "%i", WarningCount);
		DefineTable.Replace("_WARNINGS", count);
	}

	if (pass > LASTPASS) {
		SPRINTF1(ep, LINEMAX2, "warning: %s", fout);
	} else {
		SPRINTF3(ep, LINEMAX2, "%s(%lu):: warning: %s", filename, CurrentLocalLine, fout);
	}

	if (bd) {
		STRCAT(ep, LINEMAX2, ": ");
		STRCAT(ep, LINEMAX2, bd);
	}

	if (!strchr(ep, '\n')) 		STRCAT(ep, LINEMAX2, "\n");
	if (FP_ListingFile != NULL)	fputs(ErrorLine, FP_ListingFile);

	_CERR ErrorLine _END;
}




