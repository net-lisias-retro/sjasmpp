/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// sjasm.cpp

#include <sstream>

#include "VERSION.h"
#include "sjerr.h"
#include "sjasm.h"
#include "support.h"
#include "sjio.h"
#include "directives.h"
#include "options.h"
#include "lua/main.h"
#include "hw/cpu/z80.h"

CDevice *Devices = 0;
CDevice *Device = 0;
CDeviceSlot *Slot = 0;
CDevicePage *Page = 0;
char* DeviceID = 0;

// extend
char filename[LINEMAX], * lp, line[LINEMAX], temp[LINEMAX], pline[LINEMAX2], ErrorLine[LINEMAX2], * bp;
char sline[LINEMAX2], sline2[LINEMAX2];

char SourceFNames[128][MAX_PATH];
int CurrentSourceFName = 0;
int SourceFNamesCount = 0;

int ConvertEncoding = ENCWIN;

int pass = 0, IsLabelNotFound = 0, ErrorCount = 0, WarningCount = 0, IncludeLevel = -1;
int IsRunning = 0, donotlist = 0,listmacro	= 0;
int adrdisp = 0,PseudoORG = 0; /* added for spectrum ram */
char* MemoryPointer=NULL; /* added for spectrum ram */
int StartAddress = -1;
int macronummer = 0, reglenwidth = 0;
bool synerr = true, lijst = false;
aint CurAddress = 0, AddressOfMAP = 0, CurrentGlobalLine = 0, CurrentLocalLine = 0, CompiledCurrentLine = 0;
aint destlen = 0, size = (aint)-1,PreviousErrorLine = (aint)-1, maxlin = 0, comlin = 0;
char* CurrentDirectory=NULL;

void (*GetCPUInstruction)(void);

char* vorlabp=NULL, * macrolabp=NULL, * LastParsedLabel=NULL;
stack<SRepeatStack> RepeatStack;
CStringsList* lijstp = 0;
CLabelTable LabelTable;
CLocalLabelTable LocalLabelTable;
CDefineTable DefineTable;
CMacroDefineTable MacroDefineTable;
CMacroTable MacroTable;
CStructureTable StructureTable;
CAddressList* AddressList = 0; /* from SjASM 0.39g */
ModulesList Modules;

void InitPass(int p) {
	reglenwidth = 1;
	if (maxlin > 9) {
		reglenwidth = 2;
	}
	if (maxlin > 99) {
		reglenwidth = 3;
	}
	if (maxlin > 999) {
		reglenwidth = 4;
	}
	if (maxlin > 9999) {
		reglenwidth = 5;
	}
	if (maxlin > 99999) {
		reglenwidth = 6;
	}
	if (maxlin > 999999) {
		reglenwidth = 7;
	}
	if (LastParsedLabel != NULL) {
		free(LastParsedLabel);
		LastParsedLabel = NULL;
	}
	LastParsedLabel = NULL;
	vorlabp = (char *)malloc(2);
	STRCPY(vorlabp, sizeof("_"), "_");
	macrolabp = NULL;
	listmacro = 0;
	pass = p;
	CurAddress = AddressOfMAP = 0;
	IsRunning = 1;
	CurrentGlobalLine = CurrentLocalLine = CompiledCurrentLine = 0;
	PseudoORG = 0; adrdisp = 0;
	PreviousAddress = 0; epadres = 0; macronummer = 0; lijst = 0; comlin = 0;
	StructureTable.Init();
	MacroTable.Init();
	DefineTable.Init();
	MacroDefineTable.Init();

	// predefined
	if (Options::IsSjasmCompatibilityMode)		DefineTable.Replace("_SJASM", "1");
	if (Options::IsSjasmPlusCompatibilityMode)	DefineTable.Replace("_SJASMPLUS", "1");
	if (Options::IsCompassCompatibilityMode)	DefineTable.Replace("_COMPASS", "1");
	DefineTable.Replace("_SJASMPP", "1");

	{
		char buf[64] = "";
		SPRINTF2(buf, 64, "%s.%s", PRODUCT_VERSION_MAJOR, PRODUCT_VERSION_MINOR);
		DefineTable.Replace("_VERSION", buf);
		SPRINTF1(buf, 64, "%i", pass);
		DefineTable.Replace("_PASS", buf);
	}
	DefineTable.Replace("_RELEASE", PRODUCT_VERSION_RELEASE);
	DefineTable.Replace("_ERRORS", "0");
	DefineTable.Replace("_WARNINGS", "0");
}

void FreeRAM() {
	if (Devices) {
		delete Devices;
	}
	if (AddressList) {
		delete AddressList;
	}
	if (lijstp) {
		delete lijstp;
	}
	free(vorlabp);
}

void ExitASM(int p) {
	FreeRAM();
	if (pass == LASTPASS) {
		Close();
	}
	exit(p);
}

static void ShowLogo() {
	char logo[256] = "";
	SPRINTF2(logo, 256, "SjASM++ Z80 Cross-Assembler v%s (build %s)", PRODUCT_VERSION, PRODUCT_BUILT_AT);

	_COUT logo _ENDL;
}

static void ShowCopyright() {
	char copyright[256] = "";
	SPRINTF1(copyright, 256, "(c) %s Lisias Toledo retro@lisias.net https://bitbucket.org/lst_retro/sjasmpp", COPYRIGHT_YEAR);
	_COUT copyright _ENDL;

	_COUT "based on previous work by:" _ENDL;
	_COUT "\tSjASMPlus v1.07 (c) 2008 Aprisobal http://sjasmplus.sf.net" _ENDL;
	_COUT "\tSjASM v0.39 (c) 2006 Sjoerd Mastijn http://www.xl2s.tk" _ENDL;
	_COUT "with code authored by:" _ENDL;
	_COUT "\tVitamin/CAIG - vitamin.caig@gmail.com" _ENDL;
	_COUT "\tKonamiman - nestor.soriano@sunhotels.net" _ENDL;
}

static void ShowHelp() {
	_COUT "\nUsage:\nsjasmpp [options] sourcefile(s)" _ENDL;
	Options::ShowHelp();
}

static void PrintPassStatus() {
	_COUT "Pass " _CMDL pass _CMDL " complete";
	if (pass < MAXPASSES) 	_COUT " (" _CMDL ErrorCount _CMDL " errors)";
	_COUT "." _ENDL;
}

int main(int argc, const char* argv[]) {
	if (1 == argc) {
		ShowLogo();
		ShowCopyright();
		ShowHelp();
		exit(ERR_NO_INPUT);
	}

	// get arguments
	Options::IncludeDirsList.push_front(".");
	{
		int i = 1;
		if ( !Options::GetOptions(argv, i) ) {
			return ERR_INVALID_OPTION;
		}
		while (argv[i]) {
			STRCPY(SourceFNames[SourceFNamesCount++], LINEMAX, argv[i++]);
		}
	}

	if (!Options::HideLogo) {
		ShowLogo();
		if (Options::IsHelp) ShowCopyright();
	}

	if (Options::IsHelp) {
		ShowHelp();
		return ERR_NOERR;
	}

	lua::Init();
	InsertDirectives();

	if (0 == SourceFNamesCount) {
		_CERR "No inputfile(s)" _ENDL;
		return ERR_NO_INPUT;
	}

	char buf[MAX_PATH];
	int base_encoding;

	// start counter
	long dwStart;
	dwStart = GetTickCount();

	// get current directory
	GetCurrentDirectory(MAX_PATH, buf);
	CurrentDirectory = buf;

	if (Options::DestionationFName.empty()) {
		Options::DestionationFName = Filename(SourceFNames[0]).WithExtension("out");
	}

	hw::cpu::Init();

	// if memory type != none
	base_encoding = ConvertEncoding;

	// init first pass
	InitPass(1);

	// open lists
	OpenList();

	// open source filenames
	for (int i = 0; i < SourceFNamesCount; i++) {
		OpenFile(SourceFNames[i]);
	}

	PrintPassStatus();

	ConvertEncoding = base_encoding;

	do {
		pass++;

		InitPass(pass);

		if (pass == LASTPASS) {
			OpenDest();
		}
		for (int i = 0; i < SourceFNamesCount; i++) {
			OpenFile(SourceFNames[i]);
		}

		if (PseudoORG) {
			CurAddress = adrdisp; PseudoORG = 0;
		}

		PrintPassStatus();
	} while (pass < MAXPASSES);

	pass = 9999; /* added for detect end of compiling */
	if (Options::AddLabelListing) {
		//TODO: remove when FP_ListingFile will be stream
		std::ostringstream buf;
		LabelTable.Dump(buf);
		fputs(buf.str().c_str(), FP_ListingFile);
	}

	Close();

	if (!Options::UnrealLabelListFName.empty()) {
		LabelTable.DumpForUnreal(Options::UnrealLabelListFName);
	}

	if (!Options::SymbolListFName.empty()) {
		LabelTable.DumpSymbols(Options::SymbolListFName);
	}

	_COUT "Errors: " _CMDL ErrorCount _CMDL ", warnings: " _CMDL WarningCount _CMDL ", compiled: " _CMDL CompiledCurrentLine _CMDL " lines" _END;

	double dwCount;
	dwCount = GetTickCount() - dwStart;
	if (dwCount < 0) {
		dwCount = 0;
	}
	printf(", work time: %.3f seconds", (dwCount / 1000));

	_COUT "." _ENDL;

	cout << flush;

	// free RAM
	if (Devices) {
		delete Devices;
	}
	// close Lua
	lua_close(LUA);

	return (ErrorCount != 0) ? ERR_BAD_CODE : ERR_NOERR;
}
//eof sjasm.cpp
