/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Konamiman - nestor.soriano@sunhotels.net
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// sjio.cpp

#include <fcntl.h>
//#include <sys/types.h>
//#include <sys/stat.h>

#include "sjio.h"

#include "sjerr.h"
#include "options.h"
#include "reader.h"
#include "parser.h"
#include "directives.h"

#define DESTBUFLEN 8192

static char rlbuf[4096 * 2]; //x2 to prevent errors
static int RL_Readed;
static bool rldquotes = false,rlsquotes = false,rlspace = false,rlcomment = false,rlcolon = false,rlnewline = true;
static char* rlpbuf, * rlppos;

static int EB[1024 * 64],nEB = 0;
static char WriteBuffer[DESTBUFLEN];
static FILE* FP_Input = NULL, * FP_Output = NULL, * FP_RAW = NULL, * FP_ExportFile = NULL;
FILE* FP_ListingFile = NULL;
aint PreviousAddress, epadres;
static aint WBLength = 0;
static char const hd[] = {
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

void WriteDest() {
	if (!WBLength) 		return;
	destlen += WBLength;
	if (FP_Output != NULL && fwrite(WriteBuffer, 1, WBLength, FP_Output) != WBLength) {
		Error("Write error (disk full?)", 0, FATAL, ERR_FILE_IO);
	}
	if (FP_RAW != NULL && fwrite(WriteBuffer, 1, WBLength, FP_RAW) != WBLength) {
		Error("Write error (disk full?)", 0, FATAL, ERR_FILE_IO);
	}
	WBLength = 0;
}

void PrintHEX8(char*& p, aint h) {
	aint hh = h&0xff;
	*(p++) = hd[hh >> 4];
	*(p++) = hd[hh & 15];
}

void listbytes(char*& p) {
	int i = 0;
	while (nEB--) {
		PrintHEX8(p, EB[i++]); *(p++) = ' ';
	}
	i = 4 - i;
	while (i--) {
		*(p++) = ' '; *(p++) = ' '; *(p++) = ' ';
	}
}

void listbytes2(char*& p) {
	for (int i = 0; i != 5; ++i) {
		PrintHEX8(p, EB[i]);
	}
	*(p++) = ' '; *(p++) = ' ';
}

void printCurrentLocalLine(char*& p) {
	aint v = CurrentLocalLine;
	switch (reglenwidth) {
		default:
			*(p++) = (unsigned char) ('0' + v / 1000000);
			v %= 1000000;
			/* no break */
		case 6:
			*(p++) = (unsigned char) ('0' + v / 100000);
			v %= 100000;
			/* no break */
		case 5:
			*(p++) = (unsigned char) ('0' + v / 10000);
			v %= 10000;
			/* no break */
		case 4:
			*(p++) = (unsigned char) ('0' + v / 1000);
			v %= 1000;
			/* no break */
		case 3:
			*(p++) = (unsigned char) ('0' + v / 100);
			v %= 100;
			/* no break */
		case 2:
			*(p++) = (unsigned char) ('0' + v / 10);
			v %= 10;
			/* no break */
		case 1:
			*(p++) = (unsigned char) ('0' + v);
			/* no break */
	}
	*(p++) = IncludeLevel > 0 ? '+' : ' ';
	*(p++) = IncludeLevel > 1 ? '+' : ' ';
	*(p++) = IncludeLevel > 2 ? '+' : ' ';
}

void PrintHEX32(char*& p, aint h) {
	aint hh = h&0xffffffff;
	*(p++) = hd[hh >> 28]; hh &= 0xfffffff;
	*(p++) = hd[hh >> 24]; hh &= 0xffffff;
	*(p++) = hd[hh >> 20]; hh &= 0xfffff;
	*(p++) = hd[hh >> 16]; hh &= 0xffff;
	*(p++) = hd[hh >> 12]; hh &= 0xfff;
	*(p++) = hd[hh >> 8];  hh &= 0xff;
	*(p++) = hd[hh >> 4];  hh &= 0xf;
	*(p++) = hd[hh];
}

void PrintHEX16(char*& p, aint h) {
	aint hh = h&0xffff;
	*(p++) = hd[hh >> 12]; hh &= 0xfff;
	*(p++) = hd[hh >> 8]; hh &= 0xff;
	*(p++) = hd[hh >> 4]; hh &= 0xf;
	*(p++) = hd[hh];
}

char hd2[] = {
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

void PrintHEXAlt(char*& p, aint h) {
	aint hh = h&0xffffffff;
	if (hh >> 28 != 0) 	*(p++) = hd2[hh >> 28];

	hh &= 0xfffffff;
	if (hh >> 24 != 0) 	*(p++) = hd2[hh >> 24];

	hh &= 0xffffff;
	if (hh >> 20 != 0) 	*(p++) = hd2[hh >> 20];

	hh &= 0xfffff;
	if (hh >> 16 != 0) 	*(p++) = hd2[hh >> 16];

	hh &= 0xffff;
	*(p++) = hd2[hh >> 12]; hh &= 0xfff;
	*(p++) = hd2[hh >> 8];	hh &= 0xff;
	*(p++) = hd2[hh >> 4];	hh &= 0xf;
	*(p++) = hd2[hh];
}

void listbytes3(int pad) {
	int i = 0,t;
	char* pp,* sp = pline + 3 + reglenwidth;
	while (nEB) {
		pp = sp;
		PrintHEX16(pp, pad);
		*(pp++) = ' '; t = 0;
		while (nEB && t < 32) {
			PrintHEX8(pp, EB[i++]); --nEB; ++t;
		}
		*(pp++) = '\n'; *pp = 0;
		if (FP_ListingFile != NULL) 	fputs(pline, FP_ListingFile);
		pad += 32;
	}
}

void ListFile() {
	char* pp = pline;
	aint pad;
	if (pass != LASTPASS || donotlist) {
		donotlist = nEB = 0;
		return;
	}
	if (Options::ListingFName.empty() || FP_ListingFile == NULL) {
		return;
	}

	if ( listmacro && !nEB ) 	return;

	if ((pad = PreviousAddress) == (aint) - 1) {
		pad = epadres;
	}

	if (strlen(line) && line[strlen(line) - 1] != 10) {
		STRCAT(line, LINEMAX, "\n");
	} else {
		STRCPY(line, LINEMAX, "\n");
	}

	*pp = 0;
	printCurrentLocalLine(pp);
	PrintHEX16(pp, pad);
	*(pp++) = ' ';
	if (nEB < 5) {
		listbytes(pp);
		*pp = 0;
		if (listmacro) 	STRCAT(pp, LINEMAX2, ">");
		STRCAT(pp, LINEMAX2, line);
		fputs(pline, FP_ListingFile);
	} else if (nEB < 6) {
		listbytes2(pp); *pp = 0;
		if (listmacro) 	STRCAT(pp, LINEMAX2, ">");
		STRCAT(pp, LINEMAX2, line);
		fputs(pline, FP_ListingFile);
	} else {
		for (int i = 0; i != 12; ++i) {
			*(pp++) = ' ';
		}
		*pp = 0;
		if (listmacro) 	STRCAT(pp, LINEMAX2, ">");
		STRCAT(pp, LINEMAX2, line);
		fputs(pline, FP_ListingFile);
		listbytes3(pad);
	}
	epadres = CurAddress;
	PreviousAddress = (aint) - 1;
	nEB = 0;
}

void ListFileSkip(char* line) {
	char* pp = pline;
	aint pad;
	if (pass != LASTPASS || donotlist) {
		donotlist = nEB = 0;
		return;
	}
	if (Options::ListingFName.empty() || FP_ListingFile == NULL) {
		return;
	}
	if (listmacro) {
		return;
	}
	if ((pad = PreviousAddress) == (aint) - 1) {
		pad = epadres;
	}
	if (strlen(line) && line[strlen(line) - 1] != 10) {
		STRCAT(line, LINEMAX, "\n");
	}
	*pp = 0;
	printCurrentLocalLine(pp);
	PrintHEX16(pp, pad);
	*pp = 0;
	STRCAT(pp, LINEMAX2, "~            ");

	if (nEB) 		Error("Internal error lfs", 0, FATAL, ERR_INTERNAL);
	if (listmacro) 	STRCAT(pp, LINEMAX2, ">");

	STRCAT(pp, LINEMAX2, line);
	fputs(pline, FP_ListingFile);
	epadres = CurAddress;
	PreviousAddress = (aint) - 1;
	nEB = 0;
}

void CheckPage() {
	if (!DeviceID) 	return;

	CDeviceSlot* S;
	for (int i=0;i<Device->SlotsCount;i++) {
		S = Device->GetSlot(i);
		int realAddr = PseudoORG ? adrdisp : CurAddress;
		if (realAddr >= S->Address && ((realAddr < 65536 && realAddr < S->Address + S->Size) || (realAddr >= 65536 && realAddr <= S->Address + S->Size))) {
			MemoryPointer = S->Page->RAM + (realAddr - S->Address);
			Page = S->Page;
			return;
		}
	}

	Error("Error in CheckPage(). Please, contact with the author of this program.", 0, FATAL, ERR_INTERNAL);
}

void Emit(int byte) {
	EB[nEB++] = byte;
	if (pass == LASTPASS) {
		WriteBuffer[WBLength++] = (char) byte;
		if (WBLength == DESTBUFLEN) {
			WriteDest();
		}
		if (DeviceID) {
			if (PseudoORG) {
				if (CurAddress >= 0x10000) {
					char buf[1024];
					SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
					Error(buf, 0, FATAL, ERR_INTERNAL);
				}
				*(MemoryPointer++) = (char) byte;
				if ((MemoryPointer - Page->RAM) >= Page->Size) {
					++adrdisp; ++CurAddress;
					CheckPage();
					return;
				}
			} else {
				if (CurAddress >= 0x10000) {
					char buf[1024];
					SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
					Error(buf, 0, FATAL);
				}

				//if (!nulled) {
				*(MemoryPointer++) = (char) byte;
				//} else {
				//	MemoryPointer++;
				//}
			/*	if (CurAddress > 0xFFFE || (CurAddress > 0x7FFE && CurAddress < 0x8001) || (CurAddress > 0xBFFE && CurAddress < 0xC001)) {
					_COUT CurAddress _ENDL;
				}*/
				if ((MemoryPointer - Page->RAM) >= Page->Size) {
					++CurAddress;
					CheckPage();
					return;
				}
			}
		}
	}

	if (PseudoORG) 	++adrdisp;

	if (pass != LASTPASS && DeviceID && CurAddress >= 0x10000) {
		char buf[1024];
		SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
		Error(buf, 0, FATAL);
	}

	++CurAddress;
}

void EmitByte(int byte) {
	PreviousAddress = CurAddress;
	Emit(byte);
}

void EmitWord(int word) {
	PreviousAddress = CurAddress;
	Emit(word % 256);
	Emit(word / 256);
}

void EmitBytes(int* bytes) {
	PreviousAddress = CurAddress;
	if (*bytes == -1) 	Error("Illegal instruction", line, CATCHALL); *lp = 0;
	while (*bytes != -1) {
		Emit(*bytes++);
	}
}

void EmitWords(int* words) {
	PreviousAddress = CurAddress;
	while (*words != -1) {
		Emit((*words) % 256);
		Emit((*words++) / 256);
	}
}

void EmitBlock(aint byte, aint len, bool nulled) {
	PreviousAddress = CurAddress;
	if (len) 	EB[nEB++] = byte;
	while (len--) {
		if (pass == LASTPASS) {
			WriteBuffer[WBLength++] = (char) byte;
			if (WBLength == DESTBUFLEN) 	WriteDest();
			if (DeviceID) {
				if (PseudoORG) {
					if (CurAddress >= 0x10000) {
						char buf[1024];
						SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
						Error(buf, 0, FATAL);
					}
					if (!nulled) {
						*(MemoryPointer++) = (char) byte;
					} else {
						MemoryPointer++;
					}
					if ((MemoryPointer - Page->RAM) >= Page->Size) {
						++adrdisp; ++CurAddress;
						CheckPage();
						continue;
					}
				} else {
					if (CurAddress >= 0x10000) {
						char buf[1024];
						SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
						Error(buf, 0, FATAL);
					}
					if (!nulled) {
						*(MemoryPointer++) = (char) byte;
					} else {
						MemoryPointer++;
					}
					if ((MemoryPointer - Page->RAM) >= Page->Size) {
						++CurAddress;
						CheckPage();
						continue;
					}
				}
			}
		}

		if (PseudoORG) 	++adrdisp;

		if (pass != LASTPASS && DeviceID && CurAddress >= 0x10000) {
			char buf[1024];
			SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
			Error(buf, 0, FATAL);
		}
		++CurAddress;
	}
}

char* GetPath(char const * fname, bool const search_local_first) {
	char *dummy;
	return GetPath(fname, &dummy, search_local_first);
}

char* GetPath(char const * fname, char** filenamebegin, bool const search_local_first) {
	static char fullFilePath[MAX_PATH];

	DWORD g = search_local_first || Options::IsSjasmPlusCompatibilityMode
				? SearchPath(CurrentDirectory, fname, NULL, MAX_PATH, fullFilePath, filenamebegin)
				: 0
			;
	if (!g) {
		if (fname[0] == '<') 	fname++; // FIXME This should not be necessary at this point! Delete this and check against regression!
		for (std::list<std::string>::const_iterator it = Options::IncludeDirsList.begin(), lim = Options::IncludeDirsList.end(); it != lim; ++it) {
			if ( (g = SearchPath(it->c_str(), fname, NULL, MAX_PATH, fullFilePath, filenamebegin)) ) {
				break;
			}
		}
	}

	// There's no point in searching the file on the CurrentDir again if we already did it and didn't found it!
	if (!g && !search_local_first) 	SearchPath(CurrentDirectory, fname, NULL, MAX_PATH, fullFilePath, filenamebegin);

	char * kip = STRDUP(fullFilePath);
	if (!kip)		 	Error("No enough memory!", 0, FATAL, ERR_RUNTIME);
	if (filenamebegin) 	*filenamebegin += kip - fullFilePath;

	return kip;
}

void BinIncFile(char const * fname, int offset, int len) {
	char* bp;
	FILE* bif;
	int res;
	int leng;

	{
		char* fullFilePath;
		fullFilePath = GetPath(fname, (char*)NULL);
		if (*fname == '<') 	fname++;
		if (!FOPEN_ISOK(bif, fullFilePath, "rb")) 	Error("Error opening file", fname, FATAL, ERR_FILE_OPEN);
		free(fullFilePath);
	}

	if (offset > 0) {
		if ( NULL == (bp = new char[offset + 1]) ) 		Error("No enough memory!", 0, FATAL, ERR_RUNTIME);
		if ( -1 == (res = fread(bp, 1, offset, bif)) ) 	Error("Read error", fname, FATAL, ERR_FILE_ERROR);
		if ( res != offset ) 							Error("Offset beyond filelength", fname, FATAL, ERR_FILE_ERROR);
	}
	if (len > 0) {
		if ( NULL == (bp = new char[len + 1]) )			Error("No enough memory!", 0, FATAL, ERR_INTERNAL);
		if (-1 == (res = fread(bp, 1, len, bif)) ) 		Error("Read error", fname, FATAL, ERR_FILE_ERROR);
		if ( res != len )								Error("Unexpected end of file", fname, FATAL, ERR_FILE_ERROR);
		while (len--) {
			if (pass == LASTPASS) {
				WriteBuffer[WBLength++] = *bp;
				if (WBLength == DESTBUFLEN) 	WriteDest();
				if (DeviceID) {
					if (PseudoORG) {
						if (CurAddress >= 0x10000) {
							char buf[1024];
							SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
							Error(buf, 0, FATAL);
						}
						*(MemoryPointer++) = *bp;
						if ((MemoryPointer - Page->RAM) >= Page->Size) {
							++adrdisp;
							++CurAddress;
							CheckPage();
							continue;
						}
					} else {
						if (CurAddress >= 0x10000) {
							char buf[1024];
							SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
							Error(buf, 0, FATAL);
						}
						*(MemoryPointer++) = *bp;
						if ((MemoryPointer - Page->RAM) >= Page->Size) {
							++CurAddress;
							CheckPage();
							continue;
						}
					}
				}
				bp++;
			}

			if (PseudoORG) 	++adrdisp;

			if (pass != LASTPASS && DeviceID && CurAddress >= 0x10000) {
				char buf[1024];
				SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
				Error(buf, 0, FATAL);
			}
			++CurAddress;
		}
	} else {
		if (pass == LASTPASS) 	WriteDest();
		do {
			if ( -1 == (res = fread(WriteBuffer, 1, DESTBUFLEN, bif)) )		Error("Read error", fname, FATAL, ERR_FILE_ERROR);
			if (pass == LASTPASS) {
				WBLength = res;
				if (DeviceID) {
					leng = 0;
					while (leng != res) {
						if (PseudoORG) {
							if (CurAddress >= 0x10000) 	Error("RAM limit exceeded", 0, FATAL);
							*(MemoryPointer++) = (char) WriteBuffer[leng++];
							if ((MemoryPointer - Page->RAM) >= Page->Size) {
								++adrdisp; ++CurAddress;
								CheckPage();
							} else {
								++adrdisp; ++CurAddress;
							}
						} else {
							if (CurAddress >= 0x10000) 	Error("RAM limit exceeded", 0, FATAL);
							*(MemoryPointer++) = (char) WriteBuffer[leng++];
							if ((MemoryPointer - Page->RAM) >= Page->Size) {
								++CurAddress;
								CheckPage();
							} else {
								++CurAddress;
							}
						}
					}
				}
				WriteDest();
			}
			if (!DeviceID || pass != LASTPASS) {
				if (PseudoORG) 	adrdisp += res;
				for (int j=0;j < res;j++) {
					if (pass != LASTPASS && DeviceID && CurAddress >= 0x10000) {
						char buf[1024];
						SPRINTF1(buf, 1024, "RAM limit exceeded %lu", CurAddress);
						Error(buf, 0, FATAL);
					}
					++CurAddress;
				}
			}
		} while (res == DESTBUFLEN);
	}
	fclose(bif);
}

void OpenFile(char const * nfilename, bool const search_local_first) {
	char ofilename[LINEMAX];
	char* oCurrentDirectory, * fullpath;
	char* filenamebegin;

	if (++IncludeLevel > 20) 	Error("Over 20 files nested", 0, FATAL);

	fullpath = GetPath(nfilename, &filenamebegin);

	if (*nfilename == '<') 	nfilename++; // FIXME This should not be necessary! Delete and verify regression.

	if (!FOPEN_ISOK(FP_Input, fullpath, "r")) {
		free(fullpath);
		Error("Error opening file", nfilename, FATAL, ERR_FILE_OPEN);
	}

	aint oCurrentLocalLine = CurrentLocalLine;
	CurrentLocalLine = 0;
	STRCPY(ofilename, LINEMAX, filename);
	STRCPY(filename, LINEMAX, Options::IsShowFullPath ? fullpath : nfilename);

	oCurrentDirectory = CurrentDirectory;
	*filenamebegin = 0;
	CurrentDirectory = fullpath;

	// Free memory
	free(fullpath);

	RL_Readed = 0; rlpbuf = rlbuf;
	ReadBufLine(true);

	fclose(FP_Input);
	--IncludeLevel;
	CurrentDirectory = oCurrentDirectory;
	STRCPY(filename, LINEMAX, ofilename);
	if (CurrentLocalLine > maxlin) 	maxlin = CurrentLocalLine;
	CurrentLocalLine = oCurrentLocalLine;
}

void IncludeFile(char const * nfilename, bool const search_local_first) {
	FILE* oFP_Input = FP_Input;
	FP_Input = 0;

	char* pbuf = rlpbuf;
	char* buf = STRDUP(rlbuf);
	if (buf == NULL) 		Error("No enough memory!", 0, FATAL, ERR_RUNTIME);
	int readed = RL_Readed;
	bool squotes = rlsquotes,dquotes = rldquotes,space = rlspace,comment = rlcomment,colon = rlcolon,newline = rlnewline;

	rldquotes = false; rlsquotes = false;rlspace = false;rlcomment = false;rlcolon = false;rlnewline = true;

	memset(rlbuf, 0, 8192);

	OpenFile(nfilename, search_local_first);

	rlsquotes = squotes,rldquotes = dquotes,rlspace = space,rlcomment = comment,rlcolon = colon,rlnewline = newline;
	rlpbuf = pbuf;
	STRCPY(rlbuf, 8192, buf);
	RL_Readed = readed;

	free(buf);

	FP_Input = oFP_Input;
}

void ReadBufLine(bool Parse, bool SplitByColon) {
	rlppos = line;
	if (rlcolon) 	*(rlppos++) = '\t';
	while (IsRunning && (RL_Readed > 0 || (RL_Readed = fread(rlbuf, 1, 4096, FP_Input)))) {
		if (!*rlpbuf) 	rlpbuf = rlbuf;
		while (RL_Readed > 0) {
			if (*rlpbuf == '\n' || *rlpbuf == '\r') {
				if (*rlpbuf == '\n') {
					rlpbuf++; RL_Readed--;
					if (*rlpbuf && *rlpbuf == '\r') {
						rlpbuf++; RL_Readed--;
					}
				} else if (*rlpbuf == '\r') {
					rlpbuf++;RL_Readed--;
                    if (*rlpbuf && *rlpbuf == '\n') {
						rlpbuf++; RL_Readed--;
                    }
				}
				*rlppos = 0;
				if (strlen(line) == LINEMAX - 1) 	Error("Line too long", 0, FATAL);
				//if (rlnewline) {
					CurrentLocalLine++;
					CompiledCurrentLine++;
					CurrentGlobalLine++;
				//}
				rlsquotes = rldquotes = rlcomment = rlspace = rlcolon = false;
				//_COUT line _ENDL;
				if (Parse) {
					ParseLine();
				} else {
					return;
				}
				rlppos = line;
				if (rlcolon) 	*(rlppos++) = ' ';
				rlnewline = true;
			} else if (SplitByColon && *rlpbuf == ':' && rlspace && !rldquotes && !rlsquotes && !rlcomment) {
				while (*rlpbuf && *rlpbuf == ':') {
					rlpbuf++; RL_Readed--;
				}
				*rlppos = 0;
				if (strlen(line) == LINEMAX - 1) 	Error("Line too long", 0, FATAL);
				/*if (rlnewline) {
					CurrentLocalLine++; CurrentLine++; CurrentGlobalLine++; rlnewline = false;
				}*/
				rlcolon = true;
				if (Parse) {
					ParseLine();
				} else {
					return;
				}
				rlppos = line;
				if (rlcolon) 	*(rlppos++) = ' ';
			} else if (*rlpbuf == ':' && !rlspace && !rlcolon && !rldquotes && !rlsquotes && !rlcomment) {
				lp = line; *rlppos = 0; char* n;
				if ((n = getinstr(lp)) && DirectivesTable.Find(n)) {
					//it's directive
					while (*rlpbuf && *rlpbuf == ':') {
						rlpbuf++;RL_Readed--;
					}
					if (strlen(line) == LINEMAX - 1) 	Error("Line too long", 0, FATAL);
					if (rlnewline) {
						CurrentLocalLine++;
						CompiledCurrentLine++;
						CurrentGlobalLine++;
						rlnewline = false;
					}
					rlcolon = true;
					if (Parse) {
						ParseLine();
					} else {
						return;
					}
					rlspace = true;
					rlppos = line;
					if (rlcolon) 	*(rlppos++) = ' ';
				} else {
					//it's label
					*(rlppos++) = ':';
					*(rlppos++) = ' ';
					rlspace = true;
					while (*rlpbuf && *rlpbuf == ':') {
						rlpbuf++;RL_Readed--;
					}
				}
			} else {
				if (*rlpbuf == '\'' && !rldquotes && !rlcomment) 		rlsquotes = !rlsquotes;
				else if (*rlpbuf == '"' && !rlsquotes && !rlcomment) 	rldquotes = !rldquotes;
				else if (*rlpbuf == ';' && !rlsquotes && !rldquotes) 	rlcomment = true;
				else if (*rlpbuf == '/' && *(rlpbuf + 1) == '/' && !rlsquotes && !rldquotes) {
					rlcomment = true;
					*(rlppos++) = *(rlpbuf++); RL_Readed--;
				} else if (*rlpbuf <= ' ' && !rlsquotes && !rldquotes && !rlcomment) rlspace = true;
				*(rlppos++) = *(rlpbuf++); RL_Readed--;
			}
		}
		rlpbuf = rlbuf;
	}
	//for end line
	if (feof(FP_Input) && RL_Readed <= 0) {
		if (rlnewline) {
			CurrentLocalLine++;
			CompiledCurrentLine++;
			CurrentGlobalLine++;
		}
		rlsquotes = rldquotes = rlcomment = rlspace = rlcolon = false;
		rlnewline = true;
		*rlppos = 0;
		if (Parse) {
			ParseLine();
		} else {
			return;
		}
		rlppos = line;
	}
}

void OpenList() {
	if (!Options::ListingFName.empty()) {
		if (!FOPEN_ISOK(FP_ListingFile, Options::ListingFName.c_str(), "w")) {
			Error("Error opening file", Options::ListingFName.c_str(), FATAL, ERR_FILE_OPEN);
		}
	}
}

void CloseDest() {
	// simple check
	if (FP_Output == NULL) 		return;

	if (WBLength) 				WriteDest();

	if (size != -1) {
		if (destlen > size) {
			Error("File exceeds 'size'", 0);
		} else {
			long pad;
			pad = size - destlen;
			if (pad > 0) {
				while (pad--) {
					WriteBuffer[WBLength++] = 0;
					if (WBLength == 256) {
						WriteDest();
					}
				}
			}
			if (WBLength) 	WriteDest();
		}
	}
	fclose(FP_Output);
}

void SeekDest(long offset, int method) {
	WriteDest();
	if (FP_Output != NULL && fseek(FP_Output, offset, method)) {
		Error("File seek error (FORG)", 0, FATAL, ERR_FILE_ERROR);
	}
}

void NewDest(char const * newfilename) {
	NewDest(newfilename, OUTPUT_TRUNCATE);
}

void NewDest(char const * newfilename, int mode) {
	// close file
	CloseDest();

	// and open new file
	Options::DestionationFName = Filename(newfilename);
	OpenDest(mode);
}

void OpenDest() {
	OpenDest(OUTPUT_TRUNCATE);
}

void OpenDest(int mode) {
	destlen = 0;
	if (mode != OUTPUT_TRUNCATE && !FileExists(Options::DestionationFName.c_str())) {
		mode = OUTPUT_TRUNCATE;
	}
	if (!Options::NoDestinationFile && !FOPEN_ISOK(FP_Output, Options::DestionationFName.c_str(), mode == OUTPUT_TRUNCATE ? "wb" : "r+b")) {
		Error("Error opening file", Options::DestionationFName.c_str(), FATAL, ERR_FILE_IO);
	}
	Options::NoDestinationFile = false;
	if (FP_RAW == NULL && !Options::RAWFName.empty() && !FOPEN_ISOK(FP_RAW, Options::RAWFName.c_str(), "wb")) {
		Error("Error opening file", Options::RAWFName.c_str(), FATAL, ERR_FILE_IO);
	}
	if (FP_Output != NULL && mode != OUTPUT_TRUNCATE) {
		if (fseek(FP_Output, 0, mode == OUTPUT_REWIND ? SEEK_SET : SEEK_END)) {
			Error("File seek error (OUTPUT)", 0, FATAL, ERR_FILE_ERROR);
		}
	}
}

bool FileExists(char const * filename) {
	bool exists = false;
	FILE* test;
	if (FOPEN_ISOK(test, filename, "r")) {
		exists = true;
		fclose(test);
	}
	return exists;
}

void Close() {
	CloseDest();
	if (FP_ExportFile != NULL) {
		fclose(FP_ExportFile);
		FP_ExportFile = NULL;
	}
	if (FP_RAW != NULL) {
		fclose(FP_RAW);
		FP_RAW = NULL;
	}
	if (FP_ListingFile != NULL) {
		fclose(FP_ListingFile);
		FP_ListingFile = NULL;
	}
	//if (FP_UnrealList && pass == 9999) {
	//	fclose(FP_UnrealList);
	//}
}

bool SaveRAM(FILE* ff, int start, int length) {
	//unsigned int addadr = 0,save = 0;
	if (!DeviceID) 					return 0;
	if (length + start > 0xFFFF) 	length = -1;
	if (length <= 0) 				length = 0x10000 - start;

	CDeviceSlot* S;
	for (int i=0;i<Device->SlotsCount;i++) {
		S = Device->GetSlot(i);
		if (start >= S->Address	 && start < S->Address + S->Size) {
			aint const save = (length < S->Size - (start - S->Address)) ? length : S->Size - (start - S->Address);
			if ( save != fwrite(S->Page->RAM + (start - S->Address), 1, save, ff) ) 	return false;
			length -= save;
			start += save;
			//_COUT "Start: " _CMDL start _CMDL " Length: " _CMDL length _ENDL;
			if (length <= 0) 	return true;
		}
	}

	return true;
}

void* SaveRAM(void* dst, int start, int length) {
	if (!DeviceID) 					return 0;
	if (length + start > 0xFFFF) 	length = -1;
	if (length <= 0) 				length = 0x10000 - start;

	unsigned char* target = static_cast<unsigned char*>(dst);

	CDeviceSlot* S;
	for (int i=0;i<Device->SlotsCount;i++) {
		S = Device->GetSlot(i);
		if (start >= S->Address	 && start < S->Address + S->Size) {
			aint const save = (length < S->Size - (start - S->Address)) ? length : S->Size - (start - S->Address);
			std::memcpy(target, S->Page->RAM + (start - S->Address), save);
			target += save;
			length -= save;
			start += save;
			if (length <= 0) 	break;
		}
	}
	return target;
}

unsigned int MemGetWord(unsigned int address) {
	if (pass != LASTPASS) 		return 0;
	return MemGetByte(address)+(MemGetByte(address+1)*256);
}

unsigned char MemGetByte(unsigned int address) {
	if (!DeviceID || pass != LASTPASS) 		return 0;

	for (int i=0;i<Device->SlotsCount;i++) {
		CDeviceSlot * const S = Device->GetSlot(i);
		if (address >= S->Address  && address < S->Address + S->Size) {
			return S->Page->RAM[address - S->Address];
		}
	}

	Warning("Error with MemGetByte!", 0);
	ExitASM(ERR_INTERNAL);
	return 0;
}

bool SaveBinary(char const * fname, int start, int length) {
	FILE* ff;
	if (!FOPEN_ISOK(ff, fname, "wb")) 		Error("Error opening file", fname, FATAL, ERR_FILE_OPEN);
	if (length + start > 0xFFFF) 			length = -1;
	if (length <= 0) 						length = 0x10000 - start;

	//_COUT "Start: " _CMDL start _CMDL " Length: " _CMDL length _ENDL;

	fclose(ff);
	return SaveRAM(ff, start, length);
}

EReturn ReadFile(char const * pp, char const * err) {
	CStringsList* ol;
	char* p;
	while (RL_Readed > 0 || !feof(FP_Input)) {
		if (!IsRunning) 		return END;
		if (lijst) {
			if (!lijstp) 		return END;
			//p = STRCPY(line, LINEMAX, lijstp->string); //mmm
			STRCPY(line, LINEMAX, lijstp->string);
			p = line;
			ol = lijstp;
			lijstp = lijstp->next;
		} else {
			ReadBufLine(false);
			p = line;
			//_COUT "RF:" _CMDL rlcolon _CMDL line _ENDL;
		}

		SkipBlanks(p);
		if (*p == '.') 	++p;

		if (cmphstr(p, "endif")) 					{ lp = ReplaceDefine(p); return ENDIF; }
		if (cmphstr(p, "else")) 					{ ListFile(); lp = ReplaceDefine(p); return ELSE; }
		if (cmphstr(p, "endt")) 					{ lp = ReplaceDefine(p); return ENDTEXTAREA; }
		if (cmphstr(p, "dephase")) 					{ lp = ReplaceDefine(p); return ENDTEXTAREA; } // hmm??
		if (cmphstr(p, "unphase")) 					{ lp = ReplaceDefine(p); return ENDTEXTAREA; } // hmm??

		if (Options::IsCompassCompatibilityMode && cmphstr(p, "endc")) 	{ return ENDIF; }

		ParseLineSafe();
	}
	Error("Unexpected end of file", 0, FATAL);
	return END;
}

EReturn SkipFile(const char *pp, const char *err) {
	CStringsList* ol;
	char* p;
	int iflevel = 0;
	while (RL_Readed > 0 || !feof(FP_Input)) {
		if (!IsRunning) 		return END;
		if (lijst) {
			if (!lijstp) 		return END;

			//p = STRCPY(line, LINEMAX, lijstp->string); //mmm
			STRCPY(line, LINEMAX, lijstp->string);
			p = line;
			ol = lijstp;
			lijstp = lijstp->next;
		} else {
			ReadBufLine(false);
			p = line;
			//_COUT "SF:" _CMDL rlcolon _CMDL line _ENDL;
		}
		SkipBlanks(p);
		if (*p == '.') 				++p;
		if (cmphstr(p, "if")) 		++iflevel;
		if (cmphstr(p, "ifn")) 		++iflevel;
		if (cmphstr(p, "ifused")) 	++iflevel;
		if (cmphstr(p, "ifnused")) 	++iflevel;
//		if (cmphstr(p,"ifexist"))	++iflevel; TODO Checar se é interessante ressuscitar este token
//		if (cmphstr(p,"ifnexist"))	++iflevel; TODO Checar se é interessante ressuscitar este token
		if (cmphstr(p, "ifdef"))	++iflevel;
		if (cmphstr(p, "ifndef")) 	++iflevel;
		if (cmphstr(p, "endif")) {
			if (iflevel) {
				--iflevel;
			} else {
				lp = ReplaceDefine(p);
				return ENDIF;
			}
		}
		if (cmphstr(p, "else")) {
			if (!iflevel) {
				ListFile();
				lp = ReplaceDefine(p);
				return ELSE;
			}
		}
		ListFileSkip(line);
	}
	Error("Unexpected end of file", 0, FATAL);
	return END;
}

bool ReadLine(bool SplitByColon) {
	if (!IsRunning) 		return false;

	bool res = (RL_Readed > 0 || !feof(FP_Input));
	ReadBufLine(false, SplitByColon);
	return res;
}

bool ReadFileToCStringsList(CStringsList*& f, char const * end) {
	CStringsList* s,* l = NULL;
	char* p;
	f = NULL;
	while (RL_Readed > 0 || !feof(FP_Input)) {
		if (!IsRunning) 	return false;
		ReadBufLine(false);
		p = line;
		if (*p) {
			SkipBlanks(p);
			if (*p == '.')	++p;
			if (cmphstr(p, end)) {
				lp = ReplaceDefine(p);
				return true;
			}
		}
		s = new CStringsList(line, NULL);
		if (!f) 	f = s;
		if (l)		l->next = s;
		l = s;
		ListFileSkip(line);
	}
	Error("Unexpected end of file", 0, FATAL);
	return false;
}

void WriteExp(char* n, aint v) {
	char lnrs[16],* l = lnrs;
	if (FP_ExportFile == NULL) {
		if (!FOPEN_ISOK(FP_ExportFile, Options::ExportFName.c_str(), "w")) {
			Error("Error opening file", Options::ExportFName.c_str(), FATAL, ERR_FILE_OPEN);
		}
	}
	STRCPY(ErrorLine, LINEMAX2, n);
	STRCAT(ErrorLine, LINEMAX2, ": EQU ");
	STRCAT(ErrorLine, LINEMAX2, "0x");
	PrintHEX32(l, v); *l = 0;
	STRCAT(ErrorLine, LINEMAX2, lnrs);
	STRCAT(ErrorLine, LINEMAX2, "\n");
	fputs(ErrorLine, FP_ExportFile);
}

//eof sjio.cpp
