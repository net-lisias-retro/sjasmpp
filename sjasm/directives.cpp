/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Konamiman - nestor.soriano@sunhotels.net
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

// direct.cpp

#include <cctype>

#include "directives.h"

#include "sjerr.h"
#include "sjasm.h"
#include "reader.h"
#include "parser.h"
#include "options.h"
#include "sjio.h"

#include "hw/cpu/main.h"
#include "lua/directives.h"
#include "hw/devices/zx/spectrum/directives.h"
#include "modes/sjasm/directives.h"
#include "modes/sjasmplus/directives.h"
#include "modes/compass/directives.h"

CFunctionTable DirectivesTable;

bool ParseDirective(bool bol) {
	char* olp = lp;
	char* n;
	bp = lp;
	if (!(n = getinstr(lp))) {
		if (*lp == '#' && *(lp + 1) == '#') {
			lp += 2;
			aint val;
			synerr = false;
			if (!ParseExpression(lp, val)) {
				val = 4;
				synerr = true;
			}
			AddressOfMAP += ((~AddressOfMAP + 1) & (val - 1));
			return true;
		} else {
			lp = olp;
			return false;
		}
	}

	if (DirectivesTable.zoek(n, bol)) 	return true;
	else if ((!bol || Options::IsPseudoOpBOF) && *n == '.' && (isdigit((unsigned char) * (n + 1)) || *lp == '(')) {
		aint val;
		if (isdigit((unsigned char) * (n + 1))) {
			++n;
			if (!ParseExpression(n, val)) {
				Error("Syntax error", 0, CATCHALL); lp = olp; return false;
			}
		} else if (*lp == '(') {
			if (!ParseExpression(lp, val)) {
				Error("Syntax error", 0, CATCHALL); lp = olp; return false;
			}
		} else {
			lp = olp; return false;
		}
		if (val < 1) {
			Error(".X must be positive integer", 0, CATCHALL); lp = olp; return false;
		}

		char mline[LINEMAX2];
		int olistmacro; char* ml;
		char* pp = mline; *pp = 0;
		STRCPY(pp, LINEMAX2, " ");

		SkipBlanks(lp);
		if (*lp) {
			STRCAT(pp, LINEMAX2, lp);
			lp += strlen(lp);
		}
		//_COUT pp _ENDL;
		olistmacro = listmacro;
		listmacro = 1;
		ml = STRDUP(line);
		if (ml == NULL) {
			Error("No enough memory!", 0, FATAL, ERR_RUNTIME);
		}
		do {
			STRCPY(line, LINEMAX, pp);
			ParseLineSafe();
		} while (--val);
		STRCPY(line, LINEMAX, ml);
		listmacro = olistmacro;
		donotlist = 1;

		delete[] ml;
		return true;
	}
	lp = olp;
	return false;
}

bool ParseDirective_REPT() {
	char* olp = lp;
	char* n;
	bp = lp;
	if (!(n = getinstr(lp))) {
		if (*lp == '#' && *(lp + 1) == '#') {
			lp += 2;
			aint val;
			synerr = false;
			if (!ParseExpression(lp, val)) {
				val = 4;
				synerr = true;
			}
			AddressOfMAP += ((~AddressOfMAP + 1) & (val - 1));
			return true;
		} else {
			lp = olp;
			return false;
		}
	}

	if (DirectivesTable_dup.zoek(n)) {
		return true;
	}
	lp = olp;
	return false;
}

// V0.39 missings
//dirPOOL
//dirTEXT
//dirDATA

static void dirBYTE() {
	int teller, e[256];
	teller = GetBytes(lp, e, 0, 0);
	if (!teller) {
		Error("BYTE/DEFB/DB with no arguments", 0); return;
	}
	EmitBytes(e);
}

static void dirDC() {
	int teller, e[129];
	teller = GetBytes(lp, e, 0, 1);
	if (!teller) {
		Error("DC with no arguments", 0); return;
	}
	EmitBytes(e);
}

static void dirDZ() {
	int teller, e[130];
	teller = GetBytes(lp, e, 0, 0);
	if (!teller) {
		Error("DZ with no arguments", 0); return;
	}
	e[teller++] = 0; e[teller] = -1;
	EmitBytes(e);
}

static void dirABYTE() {
	aint add;
	int teller = 0,e[129];
	if (ParseExpression(lp, add)) {
		check8(add); add &= 255;
		teller = GetBytes(lp, e, add, 0);
		if (!teller) {
			Error("ABYTE with no arguments", 0); return;
		}
		EmitBytes(e);
	} else {
		Error("[ABYTE] Expression expected", 0);
	}
}

static void dirABYTEC() {
	aint add;
	int teller = 0,e[129];
	if (ParseExpression(lp, add)) {
		check8(add); add &= 255;
		teller = GetBytes(lp, e, add, 1);
		if (!teller) {
			Error("ABYTEC with no arguments", 0); return;
		}
		EmitBytes(e);
	} else {
		Error("[ABYTEC] Expression expected", 0);
	}
}

static void dirABYTEZ() {
	aint add;
	int teller = 0,e[129];
	if (ParseExpression(lp, add)) {
		check8(add); add &= 255;
		teller = GetBytes(lp, e, add, 0);
		if (!teller) {
			Error("ABYTEZ with no arguments", 0); return;
		}
		e[teller++] = 0; e[teller] = -1;
		EmitBytes(e);
	} else {
		Error("[ABYTEZ] Expression expected", 0);
	}
}

static void dirWORD() {
	aint val;
	int teller = 0,e[129];
	SkipBlanks();
	while (*lp) {
		if (ParseExpression(lp, val)) {
			check16(val);
			if (teller > 127) {
				Error("Over 128 values in DW/DEFW/WORD", 0, FATAL, ERR_RUNTIME);
			}
			e[teller++] = val & 65535;
		} else {
			Error("[DW/DEFW/WORD] Syntax error", lp, CATCHALL); return;
		}
		SkipBlanks();
		if (*lp != ',') {
			break;
		}
		++lp; SkipBlanks();
	}
	e[teller] = -1;
	if (!teller) {
		Error("DW/DEFW/WORD with no arguments", 0); return;
	}
	EmitWords(e);
}

static void dirDWORD() {
	aint val;
	int teller = 0,e[129 * 2];
	SkipBlanks();
	while (*lp) {
		if (ParseExpression(lp, val)) {
			if (teller > 127) {
				Error("[DWORD] Over 128 values", 0, FATAL, ERR_RUNTIME);
			}
			e[teller * 2] = val & 65535; e[teller * 2 + 1] = val >> 16; ++teller;
		} else {
			Error("[DWORD] Syntax error", lp, CATCHALL); return;
		}
		SkipBlanks();
		if (*lp != ',') {
			break;
		}
		++lp; SkipBlanks();
	}
	e[teller * 2] = -1;
	if (!teller) {
		Error("DWORD with no arguments", 0); return;
	}
	EmitWords(e);
}

static void dirD24() {
	aint val;
	int teller = 0,e[129 * 3];
	SkipBlanks();
	while (*lp) {
		if (ParseExpression(lp, val)) {
			check24(val);
			if (teller > 127) {
				Error("[D24] Over 128 values", 0, FATAL, ERR_RUNTIME);
			}
			e[teller * 3] = val & 255; e[teller * 3 + 1] = (val >> 8) & 255; e[teller * 3 + 2] = (val >> 16) & 255; ++teller;
		} else {
			Error("[D24] Syntax error", lp, CATCHALL); return;
		}
		SkipBlanks();
		if (*lp != ',') {
			break;
		}
		++lp; SkipBlanks();
	}
	e[teller * 3] = -1;
	if (!teller) {
		Error("D24 with no arguments", 0); return;
	}
	EmitBytes(e);
}

static void dirBLOCK() {
	aint teller,val = 0;
	if (ParseExpression(lp, teller)) {
		if ((signed) teller < 0) {
			Error("Negative BLOCK?", 0, FATAL);
		}
		if (comma(lp)) {
			ParseExpression(lp, val);
		}
		EmitBlock(val, teller);
	} else {
		Error("[BLOCK] Syntax Error", lp, CATCHALL);
	}
}

static void dirORG() {
	aint val;
	if (DeviceID) {
		if (ParseExpression(lp, val)) {
			CurAddress = val;
		} else {
			Error("[ORG] Syntax error", lp, CATCHALL); return;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[ORG] Syntax error", lp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[ORG] Negative page number are not allowed", lp); return;
			} else if (val > Device->PagesCount - 1) {
				char buf[LINEMAX];
				SPRINTF1(buf, LINEMAX, "[ORG] Page number must be in range 0..%lu", Device->PagesCount - 1);
				Error(buf, 0, CATCHALL); return;
			}
			Slot->Page = Device->GetPage(val);
			//Page = Slot->Page;
		}
		CheckPage();
	} else {
		if (ParseExpression(lp, val)) {
			CurAddress = val;
		} else {
			Error("[ORG] Syntax error", 0, CATCHALL);
		}
	}
}

static void dirMAP() {
	AddressList = new CAddressList(AddressOfMAP, AddressList); /* from SjASM 0.39g */
	aint val;
	IsLabelNotFound = 0;
	if (ParseExpression(lp, val)) {
		AddressOfMAP = val;
	} else {
		Error("[MAP] Syntax error", 0, CATCHALL);
	}
	if (IsLabelNotFound) {
		Error("[MAP] Forward reference", 0, ALL);
	}
}

static void dirENDMAP() {
	if (AddressList) {
		AddressOfMAP = AddressList->val; AddressList = AddressList->next;
	} else {
		Error("ENDMAP without MAP", 0);
	}
}

static void dirALIGN() {
	aint val;
	aint byte;
	bool noexp=false;
	if (!ParseExpression(lp, val)) {
		noexp = true;
		val = 4;
	}
	switch (val) {
	case 1:
		break;
	case 2:
	case 4:
	case 8:
	case 16:
	case 32:
	case 64:
	case 128:
	case 256:
	case 512:
	case 1024:
	case 2048:
	case 4096:
	case 8192:
	case 16384:
	case 32768:
		val = (~CurAddress + 1) & (val - 1);
		if (!noexp && comma(lp)) {
			if (!ParseExpression(lp, byte)) {
				EmitBlock(0, val, true);
			} else if (byte > 255 || byte < 0) {
				Error("[ALIGN] Illegal align byte", 0); break;
			} else {
				EmitBlock(byte, val, false);
			}
		} else {
			EmitBlock(0, val, true);
		}
		break;
	default:
		Error("[ALIGN] Illegal align", 0); break;
	}
}

static void dirMODULE() {
	if (const char* name = GetID(lp)) {
		Modules.Begin(name);
	} else {
		Error("[MODULE] Syntax error", 0, CATCHALL);
	}
}

static void dirENDMODULE() {

	if (Modules.IsEmpty()) {
		Error("ENDMODULE without MODULE", 0);
	} else {
		Modules.End();
	}
}

static void dirEND() {
	char* p = lp;
	aint val;
	if (ParseExpression(lp, val)) {
		if (val > 65535 || val < 0) {
			char buf[LINEMAX];
			SPRINTF1(buf, LINEMAX, "[END] Invalid address: %x", (unsigned int)val);
			Error(buf, 0, CATCHALL); return;
		}
		StartAddress = val;
	} else {
		lp = p;
	}

	IsRunning = 0;
}

static void dirSIZE() {
	aint val;
	if (!ParseExpression(lp, val)) {
		Error("[SIZE] Syntax error", bp, CATCHALL); return;
	}
	if (1 != pass) {
		return;
	}
	if (size != (aint) - 1) {
		Error("[SIZE] Multiple sizes?", 0); return;
	}
	size = val;
}

static void dirINCBIN() {
	aint val;
	int offset = -1,length = -1;

	const Filename& fnaam = GetFileName(lp);
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCBIN] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCBIN] Negative values are not allowed", bp); return;
			}
			offset = val;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCBIN] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCBIN] Negative values are not allowed", bp); return;
			}
			length = val;
		}
	}
	BinIncFile(fnaam.c_str(), offset, length);
}

void dirTEXTAREA() {
	aint val;
	if (ParseExpression(lp, val)) {
		adrdisp = CurAddress;CurAddress = val;
	} else {
		Error("[TEXTAREA] Syntax error", 0, CATCHALL); return;
	}
	PseudoORG = 1;
}

static void dirIF() {
	// XXX : Note - the dirCOND em CompassMode is exactly the same code, with different messages.
	// I choose to do this way to prevent messing with this code, what would make further mergings difficult.
	// TODO: Find a way to prevent duplicating this code without messing up.
	aint val;
	IsLabelNotFound = 0;
	/*if (!ParseExpression(p,val)) { Error("Syntax error",0,CATCHALL); return; }*/
	if (!ParseExpression(lp, val)) {
		Error("[IF] Syntax error", 0, CATCHALL); return;
	}
	/*if (IsLabelNotFound) Error("Forward reference",0,ALL);*/
	if (IsLabelNotFound) {
		Error("[IF] Forward reference", 0, ALL);
	}

	if (val) {
		ListFile();
		switch (ReadFile(lp, "[IF] No endif")) {
		case ELSE:
			if (SkipFile(lp, "[IF] No endif") != ENDIF) Error("[IF] No endif", 0);
			break;
		case ENDIF:
			break;
		default:
			Error("[IF] No endif!", 0);
			break;
		}
	} else {
		ListFile();
		switch (SkipFile(lp, "[IF] No endif")) {
		case ELSE:
			if (ReadFile(lp, "[IF] No endif") != ENDIF) {
				Error("[IF] No endif", 0);
			}
			break;
		case ENDIF:
			break;
		default:
			Error("[IF] No endif!", 0);
			break;
		}
	}
}

static void dirELSE() {
	Error("ELSE without IF/IFN/IFUSED/IFNUSED/IFDEF/IFNDEF", 0);
}

static void dirENDIF() {
	Error("ENDIF without IF/IFN/IFUSED/IFNUSED/IFDEF/IFNDEF", 0);
}

void dirENDTEXTAREA() {
	if (!PseudoORG) {
		Error("ENDT should be after TEXTAREA", 0);return;
	}
	CurAddress = adrdisp;
	PseudoORG = 0;
}

/*static void dirENDTEXTAREA() {
  Error("ENDT without TEXTAREA",0);
}*/

static void dirINCLUDE() {
	bool search_local_first;
	const Filename& fnaam = GetFileName(lp, search_local_first);
	ListFile();
	IncludeFile(fnaam.c_str(), search_local_first);
	donotlist = 1;
}

static void dirOUTPUT() {
	const Filename& fnaam = GetFileName(lp);
	/* begin from SjASM 0.39g */
	int mode = OUTPUT_TRUNCATE;
	if (comma(lp)) {
		char modechar = (*lp) | 0x20;
		lp++;
		if (modechar == 't') {
			mode = OUTPUT_TRUNCATE;
		} else if (modechar == 'r') {
			mode = OUTPUT_REWIND;
		} else if (modechar == 'a') {
			mode = OUTPUT_APPEND;
		} else {
			Error("Syntax error", bp, CATCHALL);
		}
	}
	if (pass == LASTPASS) {
		NewDest(fnaam.c_str(), mode);
	}
}

static void dirDEFINE() {
	char* id;

	if (!(id = GetID(lp))) {
		Error("[DEFINE] Illegal syntax", 0); return;
	}

	DefineTable.Add(id, lp, 0);

	*(lp) = 0;
}

static void dirIFDEF() {
	/*char *p=line,*id;*/
	char* id;
	/* (this was cutted)
	while (true) {
	  if (!*p) Error("ifdef error",0,FATAL);
	  if (*p=='.') { ++p; continue; }
	  if (*p=='i' || *p=='I') break;
	  ++p;
	}
	if (!cmphstr(p,"ifdef")) Error("ifdef error",0,FATAL);
	*/
	EReturn res;
	if (!(id = GetID(lp))) {
		Error("[IFDEF] Illegal identifier", 0, PASS1); return;
	}

	if (DefineTable.FindDuplicate(id)) {
		ListFile();
		/*switch (res=ReadFile()) {*/
		switch (res = ReadFile(lp, "[IFDEF] No endif")) {
			/*case ELSE: if (SkipFile()!=ENDIF) Error("No endif",0); break;*/
		case ELSE:
			if (SkipFile(lp, "[IFDEF] No endif") != ENDIF) {
				Error("[IFDEF] No endif", 0);
			} break;
		case ENDIF:
			break;
			/*default: Error("No endif!",0); break;*/
		default:
			Error("[IFDEF] No endif!", 0); break;
		}
	} else {
		ListFile();
		/*switch (res=SkipFile()) {*/
		switch (res = SkipFile(lp, "[IFDEF] No endif")) {
			/*case ELSE: if (ReadFile()!=ENDIF) Error("No endif",0); break;*/
		case ELSE:
			if (ReadFile(lp, "[IFDEF] No endif") != ENDIF) {
				Error("[IFDEF] No endif", 0);
			} break;
		case ENDIF:
			break;
			/*default: Error(" No endif!",0); break;*/
		default:
			Error("[IFDEF] No endif!", 0); break;
		}
	}
	/**lp=0;*/
}

static void dirIFNDEF() {
	/*char *p=line,*id;*/
	char* id;
	/* (this was cutted)
	while (true) {
	  if (!*p) Error("ifndef error",0,FATAL);
	  if (*p=='.') { ++p; continue; }
	  if (*p=='i' || *p=='I') break;
	  ++p;
	}
	if (!cmphstr(p,"ifndef")) Error("ifndef error",0,FATAL);
	*/
	EReturn res;
	if (!(id = GetID(lp))) {
		Error("[IFNDEF] Illegal identifier", 0, PASS1); return;
	}

	if (!DefineTable.FindDuplicate(id)) {
		ListFile();
		/*switch (res=ReadFile()) {*/
		switch (res = ReadFile(lp, "[IFNDEF] No endif")) {
			/*case ELSE: if (SkipFile()!=ENDIF) Error("No endif",0); break;*/
		case ELSE:
			if (SkipFile(lp, "[IFNDEF] No endif") != ENDIF) {
				Error("[IFNDEF] No endif", 0);
			} break;
		case ENDIF:
			break;
			/*default: Error("No endif!",0); break;*/
		default:
			Error("[IFNDEF] No endif!", 0); break;
		}
	} else {
		ListFile();
		/*switch (res=SkipFile()) {*/
		switch (res = SkipFile(lp, "[IFNDEF] No endif")) {
			/*case ELSE: if (ReadFile()!=ENDIF) Error("No endif",0); break;*/
		case ELSE:
			if (ReadFile(lp, "[IFNDEF] No endif") != ENDIF) {
				Error("[IFNDEF] No endif", 0);
			} break;
		case ENDIF:
			break;
			/*default: Error("No endif!",0); break;*/
		default:
			Error("[IFNDEF] No endif!", 0); break;
		}
	}
	/**lp=0;*/
}

static void dirEXPORT() {
	aint val;
	char* n, * p;

	if (Options::ExportFName.empty()) {
		Options::ExportFName = Filename(SourceFNames[CurrentSourceFName]).WithExtension("exp");
		Warning("[EXPORT] Filename for exportfile was not indicated. Output will be in", Options::ExportFName.c_str());
	}
	if (!(n = p = GetID(lp))) {
		Error("[EXPORT] Syntax error", lp, CATCHALL); return;
	}
	if (pass != LASTPASS) {
		return;
	}
	IsLabelNotFound = 0;

	GetLabelValue(n, val);
	if (IsLabelNotFound) {
		Error("[EXPORT] Label not found", p, SUPPRESS); return;
	}
	WriteExp(p, val);
}

static void dirMACRO() {
	//if (lijst) Error("No macro definitions allowed here",0,FATAL);
	if (lijst) {
		Error("[MACRO] No macro definitions allowed here", 0, FATAL);
	}
	char* n;
	//if (!(n=GetID(lp))) { Error("Illegal macroname",0,PASS1); return; }
	if (!(n = GetID(lp))) {
		Error("[MACRO] Illegal macroname", 0, PASS1); return;
	}
	MacroTable.Add(n, lp);
}

void dirENDM() {
	insideCompassStyleMacroDefinition = false;
	if (!RepeatStack.empty()) 		dirEDUP();
	else 							Error("[ENDM] End macro without macro", 0);
}

static void dirENDS() {
	Error("[ENDS] End structure without structure", 0);
}

static void dirASSERT() {
	char* p = lp;
	aint val;
	/*if (!ParseExpression(lp,val)) { Error("Syntax error",0,CATCHALL); return; }
	if (pass==2 && !val) Error("Assertion failed",p);*/
	if (!ParseExpression(lp, val)) {
		Error("[ASSERT] Syntax error", 0, CATCHALL); return;
	}
	if (pass == LASTPASS && !val) {
		Error("[ASSERT] Assertion failed", p);
	}
	/**lp=0;*/
}

static void dirSTRUCT() {
	CStructure* st;
	int global = 0;
	aint offset = 0,bind = 0;
	char* naam;
	SkipBlanks();
	if (*lp == '@') {
		++lp; global = 1;
	}

	if (!(naam = GetID(lp)) || !strlen(naam)) {
		Error("[STRUCT] Illegal structure name", 0, PASS1); return;
	}
	if (comma(lp)) {
		IsLabelNotFound = 0;
		if (!ParseExpression(lp, offset)) {
			Error("[STRUCT] Syntax error", 0, CATCHALL); return;
		}
		if (IsLabelNotFound) {
			Error("[STRUCT] Forward reference", 0, ALL);
		}
	}
	st = StructureTable.Add(naam, offset, bind, global);
	ListFile();
	while (true) {
		if (!ReadLine()) {
			Error("[STRUCT] Unexpected end of structure", 0, PASS1); break;
		}
		lp = line; /*if (White()) { SkipBlanks(lp); if (*lp=='.') ++lp; if (cmphstr(lp,"ends")) break; }*/
		SkipBlanks(lp);
		if (*lp == '.') {
			++lp;
		} if (cmphstr(lp, "ends")) {
			break;
		 }
		ParseStructLine(st);
		ListFileSkip(line);
	}
	st->deflab();
}

static void dirFORG() {
	aint val;
	int method = SEEK_SET;
	SkipBlanks(lp);
	if ((*lp == '+') || (*lp == '-')) {
		method = SEEK_CUR;
	}
	if (!ParseExpression(lp, val)) {
		Error("[FORG] Syntax error", 0, CATCHALL);
	}
	if (pass == LASTPASS) {
		SeekDest(val, method);
	}
}

/*
static void dirBIND() {
}
static void dirREPT() {
}
*/

/* TODO:
 * 		CPU : select the CPU code generator, pushing the current one into stack.
 * 		CPU POP : reselects the last current CPU. Automatically reissued on a ENDMODULE and <EoF>.
 */

void InsertDirectives() {
	DirectivesTable.Insert_dot("assert", dirASSERT);
	DirectivesTable.Insert_dot("byte", dirBYTE);
	DirectivesTable.Insert_dot("abyte", dirABYTE);
	DirectivesTable.Insert_dot("abytec", dirABYTEC);
	DirectivesTable.Insert_dot("abytez", dirABYTEZ);
	DirectivesTable.Insert_dot("word", dirWORD);
	DirectivesTable.Insert_dot("block", dirBLOCK);
	DirectivesTable.Insert_dot("dword", dirDWORD);
	DirectivesTable.Insert_dot("d24", dirD24);
	DirectivesTable.Insert_dot("org", dirORG);
	DirectivesTable.Insert_dot("fpos",dirFORG);
	DirectivesTable.Insert_dot("map", dirMAP);
	DirectivesTable.Insert_dot("align", dirALIGN);
	DirectivesTable.Insert_dot("module", dirMODULE);
	DirectivesTable.Insert_dot("size", dirSIZE);
	DirectivesTable.Insert_dot("textarea",dirTEXTAREA);
	DirectivesTable.Insert_dot("else", dirELSE);
	DirectivesTable.Insert_dot("export", dirEXPORT);
	DirectivesTable.Insert_dot("end", dirEND);
	DirectivesTable.Insert_dot("include", dirINCLUDE);
	DirectivesTable.Insert_dot("incbin", dirINCBIN);
	DirectivesTable.Insert_dot("binary", dirINCBIN);
	DirectivesTable.Insert_dot("insert", dirINCBIN);
	DirectivesTable.Insert_dot("if", dirIF);
	DirectivesTable.Insert_dot("output", dirOUTPUT);
	DirectivesTable.Insert_dot("define", dirDEFINE);
	DirectivesTable.Insert_dot("ifdef", dirIFDEF);
	DirectivesTable.Insert_dot("ifndef", dirIFNDEF);
	DirectivesTable.Insert_dot("macro", dirMACRO);
	DirectivesTable.Insert_dot("struct", dirSTRUCT);
	DirectivesTable.Insert_dot("dc", dirDC);
	DirectivesTable.Insert_dot("dz", dirDZ);
	DirectivesTable.Insert_dot("db", dirBYTE);
	DirectivesTable.Insert_dot("dm", dirBYTE);
	DirectivesTable.Insert_dot("dw", dirWORD);
	DirectivesTable.Insert_dot("ds", dirBLOCK);
	DirectivesTable.Insert_dot("dd", dirDWORD);
	DirectivesTable.Insert_dot("defb", dirBYTE);
	DirectivesTable.Insert_dot("defw", dirWORD);
	DirectivesTable.Insert_dot("defs", dirBLOCK);
	DirectivesTable.Insert_dot("defd", dirDWORD);
	DirectivesTable.Insert_dot("defm", dirBYTE);
	DirectivesTable.Insert_dot("endmod", dirENDMODULE);
	DirectivesTable.Insert_dot("endmodule", dirENDMODULE);
	DirectivesTable.Insert_dot("endmap", dirENDMAP); /* added from SjASM 0.39g */
	//	DirectivesTable.insertd("bind",dirBIND); /* i didn't comment this */
	DirectivesTable.Insert_dot("endif", dirENDIF);
	DirectivesTable.Insert_dot("endt", dirENDTEXTAREA);
	DirectivesTable.Insert_dot("endm", dirENDM);
	DirectivesTable.Insert_dot("ends", dirENDS);

	lua::InsertDirectives();

	modes::sjasm::InsertDirectives();
	modes::sjasmplus::InsertDirectives();
	modes::compass::InsertDirectives();

	hw::devices::zx::spectrum::InsertDirectives();
}

//eof direct.cpp
