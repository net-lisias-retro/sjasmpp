/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008
		Sjoerd Mastijn - sjasm@xl2s.tk - SjASM (c) 2005-2006

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

*/

// reader.h
#ifndef __READER
#define __READER

#include "sjdefs.h"
#include "tables.h"
#include "filename.h"

#define IsNotValidIdChar(p) (!isalnum(p) && p!='_' && p!='.' && p!='?' && p!='!' && p!='#' && p!='@')
#define IsValidIdChar(p)	(isalnum(p)  || p=='_' || p=='.' || p=='?' || p=='!' || p=='#' || p=='@')
#define White() ((bool)(isspace(*lp)))

void SkipParam(char*&);
bool SkipBlanks();
void SkipBlanks(char*& p);
bool NeedEQU();
bool NeedDEFL();
bool NeedField();
char* GetID(char*& p);
char* getinstr(char*& p);
bool comma(char*& p);
bool oparen(char*& p, char c);
bool cparen(char*& p);
char* getparen(char* p);
bool check8(aint val, bool error=true); /* changes applied from SjASM 0.39g */
bool check8o(long val); /* changes applied from SjASM 0.39g */
bool check16(aint val, bool error=true); /* changes applied from SjASM 0.39g */
bool check24(aint val, bool error=true); /* changes applied from SjASM 0.39g */
bool need(char*& p, char c);
int need(char*& p, const char *c);
int needa(char*& p, char const * c1, int r1, char const * c2 = 0, int r2 = 0, char const * c3 = 0, int r3 = 0);
bool GetConstant(char*& op, aint& val);
bool GetCharConst(char*& p, aint& val);
bool GetCharConstChar(char*& op, aint& val);
bool GetCharConstCharSingle(char*& op, aint& val);
int GetBytes(char*& p, int e[], int add, int dc);
bool cmphstr(char*& p1, char const * p2);
std::string GetString(char*& p);
Filename GetFileName(char*& p);
Filename GetFileName(char*& p, bool&);
bool needcomma(char*& p);
bool needbparen(char*& p);
bool islabchar(char p);
EStructureMembers GetStructMemberId(char*& p);

#endif
//eof reader.h

