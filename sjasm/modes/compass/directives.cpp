/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Konamiman - nestor.soriano@sunhotels.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "directives.h"

#include "sjerr.h"
#include "sjasm.h"
#include "parser.h"
#include "sjio.h"
#include "options.h"
#include "modes/compass/directives.h"

extern CFunctionTable DirectivesTable;

static void dirCOND() {
	aint val;
	IsLabelNotFound = 0;
	/*if (!ParseExpression(p,val)) { Error("Syntax error",0,CATCHALL); return; }*/
	if (!ParseExpression(lp, val)) {
		Error("[COND] Syntax error", 0, CATCHALL); return;
	}
	/*if (IsLabelNotFound) Error("Forward reference",0,ALL);*/
	if (IsLabelNotFound) {
		Error("[COND] Forward reference", 0, ALL);
	}

	if (val) {
		ListFile();
		switch (ReadFile(lp, "[COND] No endc")) {
		case ELSE:
			if (SkipFile(lp, "[COND] No endc") != ENDIF) Error("[COND] No endc", 0);
			break;
		case ENDIF:
			break;
		default:
			Error("[COND] No endc", 0);
			break;
		}
	} else {
		ListFile();
		switch (SkipFile(lp, "[COND] No endc")) {
		case ELSE:
			if (ReadFile(lp, "[COND] No endc") != ENDIF) {
				Error("[COND] No endc", 0);
			}
			break;
		case ENDIF:
			break;
		default:
			Error("[COND] No endc", 0);
			break;
		}
	}
}

static void dirENDC() {
	Error("Endc without cond", 0);
}

namespace modes{ namespace compass {

void InsertDirectives() {
	if (!Options::IsCompassCompatibilityMode) return;

	DirectivesTable.Insert_dot("cond", dirCOND, true);
	DirectivesTable.Insert_dot("endc", dirENDC, true);
}

}}
