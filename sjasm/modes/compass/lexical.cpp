/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Konamiman - nestor.soriano@sunhotels.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "sjdefs.h"

#include "modes/compass/lexical.h"

void ReplaceAtToUnderscore(char * const string) {
 	char* tempLp = string;
	char c;
	bool insideDoubleQuotedString = false;
	bool insideSingleQuotedString = false;

	while ( (c = *tempLp) ) {
		if(c == '"' && !insideSingleQuotedString) {
			insideDoubleQuotedString = !insideDoubleQuotedString;
		}
		if (c == '\'' && !insideDoubleQuotedString) {
			insideSingleQuotedString = !insideSingleQuotedString;
		}
		if (c == '@' && !insideDoubleQuotedString && !insideSingleQuotedString)
			*tempLp = '_';
		tempLp++;
	}
}


