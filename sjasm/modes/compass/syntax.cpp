/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Konamiman - nestor.soriano@sunhotels.net

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include <ctype.h>

#include "syntax.h"
#include "sjasm.h"
#include "reader.h"
#include "modes/compass/syntax.h"

void ReformatCompassStyleMacro(char * const line) {
	char* labelStart;
	char* labelEnd;
	char* paramsStart;
	int i;
	int labelLength;

	char* lp = line;
	if (!*lp || *lp <= ' ')		return;

	labelStart = line;
	while (*lp && *lp > ' ' && *lp != ':') {
		lp++;
	}
	if (!*lp)					return;

	labelEnd = lp;
	labelLength = labelEnd - labelStart;

	while (*lp == ':') 			lp++;
	while (*lp && *lp <= ' ')	++lp;
	if (!*lp)					return;

	if (tolower(lp[0]) != 'm')	return;
	if (tolower(lp[1]) != 'a')	return;
	if (tolower(lp[2]) != 'c')	return;
	if (tolower(lp[3]) != 'r')	return;
	if (tolower(lp[4]) != 'o')	return;
	if (lp[5] > ' ')			return;

	lp += 5;
	while (*lp && *lp <= ' ')	++lp;
	if (!*lp)					return;

	paramsStart = lp;

	/* It's a Compass style macro */
	memcpy(temp, labelStart, labelLength);
	temp[labelLength] = '\0';

	strcpy(line, " macro ");
	strcpy(line + 7, temp);
	line[7 + labelLength] = ' ';
	strcpy(line + 7 + labelLength + 1, paramsStart);

	insideCompassStyleMacroDefinition = true;
}

void ConvertCompassStyleLocalLabels(char * const line) {
	//Replaces "label@sym" into ".labelSym"

	char* lp = line;
	char* atSymPointer;
	char* labelStartPointer;
	int labelLength;
	int i;

	while (*lp) {
		if (!(*lp == '@' && tolower(lp[1]) == 's' && tolower(lp[2]) == 'y' && tolower(lp[3]) == 'm')) {
			lp++;
			continue;
		}

		atSymPointer = lp;
		lp--;
		if(lp < line)	return;

		while(lp >= line && IsValidIdChar(*lp)) {
			lp--;
		}

		labelStartPointer = lp + 1;
		labelLength = atSymPointer - labelStartPointer;
		if (labelLength == 0) {
			lp = atSymPointer + 4;
			continue;
		}

		for (i = labelLength - 1; i >= 0; i--)
			labelStartPointer[i + 1] = labelStartPointer[i];

		*labelStartPointer = '.';
		atSymPointer[1] = 'S';

		lp = atSymPointer + 4;
	}
}
