/*
  SjASM++ Z80 Cross Compiler
  Copyright (C) 2016-18 Lisias Toledo - retro@lisias.net

  This is modified sources from SjASMPlus and SjASM.

  Authors:
		Lisias - retro@lisias.net
		Aprisobal - aprisobal@tut - SjASMPlus (c) 2004-2008

  This software is provided 'as-is', without any express or implied warranty.
  In no event will the authors be held liable for any damages arising from the
  use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it freely,
  subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not claim
	 that you wrote the original software. If you use this software in a product,
	 an acknowledgment in the product documentation would be appreciated but is
	 not required.

  2. Altered source versions must be plainly marked as such, and must not be
	 misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.
*/

#include "directives.h"

#include "sjerr.h"
#include "tables.h"
#include "sjasm.h"
#include "sjio.h"
#include "reader.h"
#include "parser.h"
#include "options.h"
#include "hw/devices/zx/spectrum/main.h"
#include "hw/devices/zx/spectrum/lexical.h"
#include "modes/compass/main.h"

extern CFunctionTable DirectivesTable;
CFunctionTable DirectivesTable_dup;
void dirTEXTAREA();
void dirENDTEXTAREA();
void dirENDM();

static void dirPAGE() {
	aint val;
	if (!DeviceID) {
		Warning("PAGE only allowed in real device emulation mode (See DEVICE)", 0);
		SkipParam(lp);
		return;
	}
	if (!ParseExpression(lp, val)) {
		Error("Syntax error", 0, CATCHALL);
		return;
	}
	if (val < 0) {
		Error("[PAGE] Negative page number are not allowed", lp); return;
	} else if (val > Device->PagesCount - 1) {
		char buf[LINEMAX];
		SPRINTF1(buf, LINEMAX, "[PAGE] Page number must be in range 0..%lu", Device->PagesCount - 1);
		Error(buf, 0, CATCHALL); return;
	}
	Slot->Page = Device->GetPage(val);
	CheckPage();
}

static void dirSLOT() {
	aint val;
	if (!DeviceID) {
		Warning("SLOT only allowed in real device emulation mode (See DEVICE)", 0);
		SkipParam(lp);
		return;
	}
	if (!ParseExpression(lp, val)) {
		Error("Syntax error", 0, CATCHALL);
		return;
	}
	if (val < 0) {
		Error("[SLOT] Negative slot number are not allowed", lp); return;
	} else if (val > Device->SlotsCount - 1) {
		char buf[LINEMAX];
		SPRINTF1(buf, LINEMAX, "[SLOT] Slot number must be in range 0..%lu", Device->SlotsCount - 1);
		Error(buf, 0, CATCHALL); return;
	}
	Slot = Device->GetSlot(val);
	Device->CurrentSlot = Slot->Number;
	CheckPage();
}

static void dirINCHOB() {
	aint val;
	char * fnaamh;
	unsigned char len[2];
	int offset = 17,length = -1,res;
	FILE* ff;

	const Filename& fnaam = GetFileName(lp);
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCHOB] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCHOB] Negative values are not allowed", bp); return;
			}
			offset += val;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCHOB] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCHOB] Negative values are not allowed", bp); return;
			}
			length = val;
		}
	}

	//used for implicit format check
	fnaamh = GetPath(fnaam.c_str(), (char*)NULL);
	if (!FOPEN_ISOK(ff, fnaamh, "rb")) {
		Error("[INCHOB] Error opening file", fnaam.c_str(), FATAL, ERR_FILE_OPEN);
	}
	if (fseek(ff, 0x0b, 0)) {
		Error("[INCHOB] Hobeta file has wrong format", fnaam.c_str(), FATAL);
	}
	res = fread(len, 1, 2, ff);
	if (res != 2) {
		Error("[INCHOB] Hobeta file has wrong format", fnaam.c_str(), FATAL);
	}
	if (length == -1) {
		length = len[0] + (len[1] << 8);
	}
	fclose(ff);
	BinIncFile(fnaam.c_str(), offset, length);
	delete[] fnaamh;
}

static void dirINCTRD() {
	aint val;
	char hdr[16];
	int offset = -1,length = -1,res,i;
	FILE* ff;

	const Filename& fnaam = GetFileName(lp);
	HobetaFilename fnaamh;
	if (comma(lp)) {
		if (!comma(lp)) {
			fnaamh = GetHobetaFileName(lp);
		} else {
			Error("[INCTRD] Syntax error", bp, CATCHALL); return;
		}
	}
	if (fnaamh.Empty()) {
		Error("[INCTRD] Syntax error", bp, CATCHALL); return;
	}
	if (comma(lp)) {
		if (!comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCTRD] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCTRD] Negative values are not allowed", bp); return;
			}
			offset += val;
		}
		if (comma(lp)) {
			if (!ParseExpression(lp, val)) {
				Error("[INCTRD] Syntax error", bp, CATCHALL); return;
			}
			if (val < 0) {
				Error("[INCTRD] Negative values are not allowed", bp); return;
			}
			length = val;
		}
	}
	//TODO: extract code to io_trd
	// open TRD
	char* fnaamh2 = GetPath(fnaam.c_str(), (char*)NULL);
	if (!FOPEN_ISOK(ff, fnaamh2, "rb")) {
		Error("[INCTRD] Error opening file", fnaam.c_str(), FATAL, ERR_FILE_OPEN);
	}
	// Find file
	fseek(ff, 0, SEEK_SET);
	for (i = 0; i < 128; i++) {
		res = fread(hdr, 1, 16, ff);
		if (res != 16) {
			Error("[INCTRD] Read error", fnaam.c_str(), CATCHALL); return;
		}
		if (0 == std::memcmp(hdr, fnaamh.GetTrDosEntry(), fnaamh.GetTrdDosEntrySize())) {
			i = 0; break;
		}
	}
	if (i) {
		Error("[INCTRD] File not found in TRD image", fnaamh.c_str(), CATCHALL); return;
	}
	if (length > 0) {
		if (offset == -1) {
			offset = 0;
		}
	} else {
		if (length == -1) {
			length = ((unsigned char)hdr[0x0b]) + (((unsigned char)hdr[0x0c]) << 8);
		}
		if (offset == -1) {
			offset = 0;
		} else {
			length -= offset;
		}
	}
	offset += (((unsigned char)hdr[0x0f]) << 12) + (((unsigned char)hdr[0x0e]) << 8);
	fclose(ff);

	BinIncFile(fnaam.c_str(), offset, length);
	delete[] fnaamh2;
}

static void dirENCODING() {
	const std::string& enc = GetString(lp);
	if (enc.empty()) {
		Error("[ENCODING] Syntax error. No parameters", bp, CATCHALL); return;
	}
	//TODO: make compare function or type
	std::string lowercased;
	for (const char* p = enc.c_str(); *p; ++p) {
		lowercased += std::tolower(*p);
	}
	if (lowercased ==  "dos") {
		ConvertEncoding = ENCDOS;
	} else if (lowercased == "win") {
		ConvertEncoding = ENCWIN;
	} else {
		Error("[ENCODING] Syntax error. Bad parameter", bp, CATCHALL);
	}
}

static void dirLABELSLIST() {
	if (!DeviceID) {
		Error("LABELSLIST only allowed in real device emulation mode (See DEVICE)", 0);
	}

	if (pass != 1 || !DeviceID) {
		SkipParam(lp);return;
	}
	const Filename& opt = GetFileName(lp);
	if (opt.empty()) {
		Error("[LABELSLIST] Syntax error. No parameters", bp, CATCHALL); return;
	}
	Options::UnrealLabelListFName = opt;
}

static void dirIFN() {
	aint val;
	IsLabelNotFound = 0;
	if (!ParseExpression(lp, val)) {
		Error("[IFN] Syntax error", 0, CATCHALL); return;
	}
	if (IsLabelNotFound) {
		Error("[IFN] Forward reference", 0, ALL);
	}

	if (!val) {
		ListFile();
		switch (ReadFile(lp, "[IFN] No endif")) {
		case ELSE:
			if (SkipFile(lp, "[IFN] No endif") != ENDIF) {
				Error("[IFN] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFN] No endif!", 0); break;
		}
	} else {
		ListFile();
		switch (SkipFile(lp, "[IFN] No endif")) {
		case ELSE:
			if (ReadFile(lp, "[IFN] No endif") != ENDIF) {
				Error("[IFN] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFN] No endif!", 0); break;
		}
	}
}

static void dirIFUSED() {
	char* id;
	if (((id = GetID(lp)) == NULL || *id == 0) && LastParsedLabel == NULL) {
		Error("[IFUSED] Syntax error", 0, CATCHALL);
		return;
	}
	if (id == NULL || *id == 0) {
		id = LastParsedLabel;
	} else {
		id = ValidateLabel(id);
		if (id == NULL) {
			Error("[IFUSED] Invalid label name", 0, CATCHALL);
			return;
		}
	}

	if (LabelTable.IsUsed(id)) {
		ListFile();
		switch (ReadFile(lp, "[IFUSED] No endif")) {
		case ELSE:
			if (SkipFile(lp, "[IFUSED] No endif") != ENDIF) {
				Error("[IFUSED] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFUSED] No endif!", 0); break;
		}
	} else {
		ListFile();
		switch (SkipFile(lp, "[IFUSED] No endif")) {
		case ELSE:
			if (ReadFile(lp, "[IFUSED] No endif") != ENDIF) {
				Error("[IFUSED] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFUSED] No endif!", 0); break;
		}
	}
}

static void dirIFNUSED() {
	char* id;
	if (((id = GetID(lp)) == NULL || *id == 0) && LastParsedLabel == NULL) {
		Error("[IFUSED] Syntax error", 0, CATCHALL);
		return;
	}
	if (id == NULL || *id == 0) {
		id = LastParsedLabel;
	} else {
		id = ValidateLabel(id);
		if (id == NULL) {
			Error("[IFUSED] Invalid label name", 0, CATCHALL);
			return;
		}
	}

	if (!LabelTable.IsUsed(id)) {
		ListFile();
		switch (ReadFile(lp, "[IFNUSED] No endif")) {
		case ELSE:
			if (SkipFile(lp, "[IFNUSED] No endif") != ENDIF) {
				Error("[IFNUSED] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFNUSED] No endif!", 0); break;
		}
	} else {
		ListFile();
		switch (SkipFile(lp, "[IFNUSED] No endif")) {
		case ELSE:
			if (ReadFile(lp, "[IFNUSED] No endif") != ENDIF) {
				Error("[IFNUSED] No endif", 0);
			} break;
		case ENDIF:
			break;
		default:
			Error("[IFNUSED] No endif!", 0); break;
		}
	}
}

static void dirUNDEFINE() {
	char* id;

	if (!(id = GetID(lp)) && *lp != '*') {
		Error("[UNDEFINE] Illegal syntax", 0); return;
	}

	if (*lp == '*') {
		lp++;
		if (pass == PASS1) {
			LabelTable.RemoveAll();
		}
		DefineTable.RemoveAll();
	} else if (DefineTable.FindDuplicate(id)) {
		DefineTable.Remove(id);
	} else if (LabelTable.Find(id)) {
		if (pass == PASS1) {
			LabelTable.Remove(id);
		}
	} else {
		Warning("[UNDEFINE] Identifier not found", 0); return;
	}
}

static void dirDISPLAY() {
	char decprint = 0;
	char e[LINEMAX];
	char* ep = e;
	aint val;
	int t = 0;
	while (1) {
		SkipBlanks(lp);
		if (!*lp) {
			Error("[DISPLAY] Expression expected", 0, PASS3); break;
		}
		if (t == LINEMAX - 1) {
			Error("[DISPLAY] Too many arguments", lp, PASS3); break;
		}
		if (*(lp) == '/') {
			++lp;
			switch (*(lp++)) {
			case 'A':
			case 'a':
				decprint = 2;break;
			case 'D':
			case 'd':
				decprint = 1;break;
			case 'H':
			case 'h':
				decprint = 0;break ;
			case 'L':
			case 'l':
				break ;
			case 'T':
			case 't':
				break ;
			default:
				Error("[DISPLAY] Syntax error", line, PASS3);return;
			}
			SkipBlanks(lp);

			if ((*(lp) != 0x2c)) {
				Error("[DISPLAY] Syntax error", line, PASS3);return;
			}
			++lp;
			SkipBlanks(lp);
		}

		if (*lp == '"') {
			lp++;
			do {
				if (!*lp || *lp == '"') {
					Error("[DISPLAY] Syntax error", line, PASS3);
					*ep = 0;
					return;
				}
				if (t == 128) {
					Error("[DISPLAY] Too many arguments", line, PASS3);
					*ep = 0;
					return;
				}
				GetCharConstChar(lp, val);
				check8(val);
				*(ep++) = (char) (val & 255);
			} while (*lp != '"');
			++lp;
		} else if (*lp == 0x27) {
			lp++;
			do {
				if (!*lp || *lp == 0x27) {
					Error("[DISPLAY] Syntax error", line, PASS3);
					*ep = 0;
					return;
				}
				if (t == LINEMAX - 1) {
					Error("[DISPLAY] Too many arguments", line, PASS3);
					*ep = 0;
					return;
				}
				GetCharConstCharSingle(lp, val);
				check8(val);
				*(ep++) = (char) (val & 255);
			} while (*lp != 0x27);
			++lp;
		} else {
			if (ParseExpression(lp, val)) {
				if (decprint == 0 || decprint == 2) {
					*(ep++) = '0';
					*(ep++) = 'x';
					if (val < 0x1000) {
						PrintHEX16(ep, val);
					} else {
						PrintHEXAlt(ep, val);
					}
				}
				if (decprint == 2) {
					*(ep++) = ',';
					*(ep++) = ' ';
				}
				if (decprint == 1 || decprint == 2) {
					SPRINTF1(ep, (int)(&e[0] + LINEMAX - ep), "%ld", val);
					ep += strlen(ep);
				}
				decprint = 0;
			} else {
				Error("[DISPLAY] Syntax error", line, PASS3);
				return;
			}
		}
		SkipBlanks(lp);
		if (*lp != ',') {
			break;
		}
		++lp;
	}
	*ep = 0; // end line

	if (pass != LASTPASS) {
		// do none
	} else {
		_COUT "> " _CMDL e _ENDL;
	}
}

static void dirSHELLEXEC() {
	const std::string& command = GetString(lp);
	const std::string& parameters = comma(lp) ? GetString(lp) : std::string();
	if (pass == LASTPASS) {
		const std::string log = command + ' ' + parameters;
		_COUT "Executing " _CMDL log _ENDL;
#if defined(WIN32)
		STARTUPINFO si;
		PROCESS_INFORMATION pi;
		ZeroMemory( &si, sizeof(si) );
		si.cb = sizeof(si);
		ZeroMemory( &pi, sizeof(pi) );

		// Start the child process.
		if (!parameters.empty()) {
			if( !CreateProcess(const_cast<char*>(command.c_str()),	 // No module name (use command line).
				const_cast<char*>(parameters.c_str()), // Command line.
				NULL,			// Process handle not inheritable.
				NULL,			// Thread handle not inheritable.
				TRUE,			// Set handle inheritance to FALSE.
				0,				// No creation flags.
				NULL,			// Use parent's environment block.
				NULL,			// Use parent's starting directory.
				&si,			// Pointer to STARTUPINFO structure.
				&pi )			// Pointer to PROCESS_INFORMATION structure.
				) {
				Error( "[SHELLEXEC] Execution of command failed", log.c_str(), PASS3 );
			} else {
				CloseHandle(pi.hThread);
				WaitForSingleObject(pi.hProcess, 500);
				CloseHandle(pi.hProcess);
			}
		} else {
			if( !CreateProcess( NULL,	// No module name (use command line).
				const_cast<char*>(command.c_str()), // Command line.
				NULL,			// Process handle not inheritable.
				NULL,			// Thread handle not inheritable.
				FALSE,			// Set handle inheritance to FALSE.
				0,				// No creation flags.
				NULL,			// Use parent's environment block.
				NULL,			// Use parent's starting directory.
				&si,			// Pointer to STARTUPINFO structure.
				&pi )			// Pointer to PROCESS_INFORMATION structure.
				) {
				Error( "[SHELLEXEC] Execution of command failed", command.c_str(), PASS3 );
			} else {
				CloseHandle(pi.hThread);
				WaitForSingleObject(pi.hProcess, 500);
				CloseHandle(pi.hProcess);
			}
		}
		//system(command);
		///WinExec ( command, SW_SHOWNORMAL );
#else
		if (system(command.c_str()) == -1) {
			Error( "[SHELLEXEC] Execution of command failed", command.c_str(), PASS3 );
		}
#endif
	}
}

static void dirDUP() {
	aint val;
	IsLabelNotFound = 0;

	if (!RepeatStack.empty()) {
		SRepeatStack& dup = RepeatStack.top();
		if (!dup.IsInWork) {
			if (!ParseExpression(lp, val)) {
				Error("[DUP/REPT] Syntax error", 0, CATCHALL);
				return;
			}
			dup.Level++;
			return;
		}
	}

	if (!ParseExpression(lp, val)) {
		Error("[DUP/REPT] Syntax error", 0, CATCHALL);
		return;
	}
	if (IsLabelNotFound) {
		Error("[DUP/REPT] Forward reference", 0, ALL);
	}
	if ((int) val < 1) {
		Error("[DUP/REPT] Illegal repeat value", 0, CATCHALL);
		return;
	}

	insideCompassStyleMacroDefinition = false; // TODO Check where else this is needed!!

	SRepeatStack dup;
	dup.RepeatCount = val;
	dup.Level = 0;

	dup.Lines = new CStringsList(lp, NULL);
	dup.Pointer = dup.Lines;
	dup.lp = lp; //чтобы брать код перед EDUP
	dup.CurrentGlobalLine = CurrentGlobalLine;
	dup.CurrentLocalLine = CurrentLocalLine;
	dup.IsInWork = false;
	RepeatStack.push(dup);
}

void dirEDUP() {
	if (RepeatStack.empty()) {
		Error("[EDUP/ENDR] End repeat without repeat", 0);
		return;
	}

	if (!RepeatStack.empty()) {
		SRepeatStack& dup = RepeatStack.top();
		if (!dup.IsInWork && dup.Level) {
			dup.Level--;
			return;
		}
	}
	int olistmacro;
	long gcurln, lcurln;
	char* ml;
	SRepeatStack& dup = RepeatStack.top();
	dup.IsInWork = true;
	dup.Pointer->string = new char[LINEMAX];
	if (dup.Pointer->string == NULL) 		Error("[EDUP/ENDR] No enough memory!", 0, FATAL, ERR_RUNTIME);
	*dup.Pointer->string = 0;
	STRNCAT(dup.Pointer->string, LINEMAX, dup.lp, lp - dup.lp - 4); //чтобы взять код перед EDUP/ENDR/ENDM
	CStringsList* s;
	olistmacro = listmacro;
	listmacro = 1;
	ml = STRDUP(line);
	if (ml == NULL) 						Error("[EDUP/ENDR] No enough memory", 0, FATAL, ERR_RUNTIME);
	gcurln = CurrentGlobalLine;
	lcurln = CurrentLocalLine;
	while (dup.RepeatCount--) {
		CurrentGlobalLine = dup.CurrentGlobalLine;
		CurrentLocalLine = dup.CurrentLocalLine;
		s = dup.Lines;
		while (s) {
			STRCPY(line, LINEMAX, s->string);
			s = s->next;
			ParseLineSafe();
			CurrentLocalLine++;
			CurrentGlobalLine++;
			CompiledCurrentLine++;
		}
	}
	RepeatStack.pop();
	CurrentGlobalLine = gcurln;
	CurrentLocalLine = lcurln;
	listmacro = olistmacro;
	donotlist = 1;
	STRCPY(line, LINEMAX,  ml);

	ListFile();
}

static void dirDEFARRAY() {
	char* n;
	char* id;
	char ml[LINEMAX];
	CStringsList* a;
	CStringsList* f;

	if (!(id = GetID(lp))) {
		Error("[DEFARRAY] Syntax error", 0); return;
	}
	SkipBlanks(lp);
	if (!*lp) {
		Error("DEFARRAY must have less one entry", 0); return;
	}

	a = new CStringsList();
	f = a;
	while (*lp) {
		n = ml;
		SkipBlanks(lp);
		if (*lp == '<') {
			++lp;
			while (*lp != '>') {
				if (!*lp) {
					Error("[DEFARRAY] No closing bracket - <..>", 0); return;
				}
				if (*lp == '!') {
					++lp; if (!*lp) {
							Error("[DEFARRAY] No closing bracket - <..>", 0); return;
						  }
				}
				*n = *lp; ++n; ++lp;
			}
			++lp;
		} else {
			while (*lp && *lp != ',') {
				*n = *lp; ++n; ++lp;
			}
		}
		*n = 0;
		//_COUT a->string _ENDL;
		f->string = STRDUP(ml);
		if (f->string == NULL) {
			Error("[DEFARRAY] No enough memory", 0, FATAL, ERR_RUNTIME);
		}
		SkipBlanks(lp);
		if (*lp == ',') {
			++lp;
		} else {
			break;
		}
		f->next = new CStringsList();
		f = f->next;
	}
	DefineTable.Add(id, "\n", a);
	//while (a) { STRCPY(ml,a->string); _COUT ml _ENDL; a=a->next; }
}


static void dirDEVICE() {
	char* id;

	if ( (id = GetID(lp)) ) {
		if (!SetDevice(id)) {
			Error("[DEVICE] Invalid parameter", 0, CATCHALL);
		}
	} else {
		Error("[DEVICE] Syntax error", 0, CATCHALL);
	}
}

namespace modes{ namespace sjasmplus {

void InsertDirectives() {
	if (!Options::IsSjasmPlusCompatibilityMode) return;

	DirectivesTable.Insert_dot("display", dirDISPLAY);
	DirectivesTable.Insert_dot("inchob", dirINCHOB);
	DirectivesTable.Insert_dot("inctrd", dirINCTRD);
	DirectivesTable.Insert_dot("shellexec", dirSHELLEXEC);
/*#ifdef WIN32
	DirectivesTable.insertd("winexec", dirWINEXEC);
#endif*/
	DirectivesTable.Insert_dot("ifn", dirIFN);
	DirectivesTable.Insert_dot("ifused", dirIFUSED);
	DirectivesTable.Insert_dot("ufnused", dirIFNUSED);
	DirectivesTable.Insert_dot("undefine", dirUNDEFINE);
	DirectivesTable.Insert_dot("defarray", dirDEFARRAY);
	DirectivesTable.Insert_dot("rept", dirDUP);
	DirectivesTable.Insert_dot("dup", dirDUP);
	DirectivesTable.Insert_dot("disp", dirTEXTAREA);
	DirectivesTable.Insert_dot("phase", dirTEXTAREA);
	DirectivesTable.Insert_dot("ent", dirENDTEXTAREA);
	DirectivesTable.Insert_dot("unphase", dirENDTEXTAREA);
	DirectivesTable.Insert_dot("dephase", dirENDTEXTAREA);
	DirectivesTable.Insert_dot("page", dirPAGE);
	DirectivesTable.Insert_dot("slot", dirSLOT);
	DirectivesTable.Insert_dot("encoding", dirENCODING);
	DirectivesTable.Insert_dot("labelslist", dirLABELSLIST);
	DirectivesTable.Insert_dot("edup", dirEDUP);
	DirectivesTable.Insert_dot("endr", dirEDUP);
	DirectivesTable.Insert_dot("device", dirDEVICE);

	DirectivesTable_dup.Insert_dot("dup", dirDUP);
	DirectivesTable_dup.Insert_dot("edup", dirEDUP);
	DirectivesTable_dup.Insert_dot("endr", dirEDUP);
	DirectivesTable_dup.Insert_dot("rept", dirDUP);
	DirectivesTable_dup.Insert_dot("endm", dirENDM);
}

}}
