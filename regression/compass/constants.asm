; Accepts_empty_string_as_zero_if_c_option_specified()
	ld a,""
	ld b,''

; Must be the same as
	ld a,0
	ld b,0



; Allow whitespace between digits
	ld a,% 11 00 11 00 ;!
 	ld a,11 00 11 00 b ;!
 	ld a,&b 11 00 11 00 ;!
 	ld bc,&h AA BB ;!
 	ld bc,0 AA BB h ;!
 	ld bc,0x AA BB ;!
 	ld de,12 34 ;!

; Must be the same as
 	ld a,%11001100
 	ld a,11001100b
 	ld a,&b11001100
 	ld bc,&hAABB
 	ld bc,0AABBh
 	ld bc,0xAABB
 	ld de,1234

