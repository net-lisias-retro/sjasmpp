extern char const * const PRODUCT_VERSION = "1.8 alpha 4";
extern char const * const PRODUCT_VERSION_MAJOR = "1";
extern char const * const PRODUCT_VERSION_MINOR = "8";
extern char const * const PRODUCT_VERSION_RELEASE = "4";
extern char const * const PRODUCT_BUILT_AT = "[PRODUCT_BUILT_AT]";
extern char const * const COPYRIGHT_YEAR = "2016-[COPYRIGHT_YEAR]";
