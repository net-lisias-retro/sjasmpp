# SjASM++ #

This is an ongoing effort to revamp and maintain a stable and coherent version of the SjASMPlus to be used by my own retro projects for Z80([*](#someprojectsusingit)).

* Homepage: [Lisias on the Net](http://retro.lisias.net/my/library/dev/sjasmpp.md)
	* with binaries! 
* Source: [Bitbucket](https://bitbucket.org/lst_retro/sjasmpp)

## Current Status ##

* Production
	+ [Version 1.8 alpha 4](https://bitbucket.org/lst_retro/sjasmpp/src/master/) (last stable release) 
+ Development
	* [Version 1.8 alpha 5](https://bitbucket.org/lst_retro/sjasmpp/src/stage/) (WiP)
	+ [Version 1.8 alpha 4](https://bitbucket.org/lst_retro/sjasmpp/commits/tag/RELEASE/2018-0221.1_8_alpha4) (last development milestone) 
* MacOS and Linux builds are working
* Windows Desktop build are not tested, and should be considered experimental for while.
* Windows CE build was dropped.

See [man page](./docs/sjasmpp.md), [TODO](TODO.md) and [ChangeLog](ChangeLog.md) for details. This project has a [ROADMAP](ROADMAP.md).

See Homepage for binaries and Source for source code.

### Binaries

No binary is committed on this code repository. You can find them (when available) at [my site's page for this project](http://retro.lisias.net/my/library/sjasmpp).

## Rationale

Aprisobal made an excellent job adding some useful features (as using Lua as a scripting engine!) and fixing some bugs, but this fork is currently marooned. Some other forks are equally staled, or have conflicting goals.

In the mean time, some new requirements arose and I need to cope with them somehow.

So the best solution ended up being creating *Yet Another Fork*™ of the tool. :-)

Some features will probably be dropped on the process - I don't have how to keep alive the WinCE port and Russian language support. Sorry. :-(

I'm aiming to fix the current bugs and to 

* backport the `MSX` support routines from the [SjASM from Sjoerd Mastijn](http://xl2s.eu.pn/sjasm.html)'s 0.42c version (SjASMPlus was forked from a previous version than 0.39h).
* "front-port" fixes and enhancements from some other forks I found (see [stage](https://bitbucket.org/lst_retro/sjasmpp/branch/stage)'s README.md, ChangedLog.md and TODO.md)
* Implement native support for the [`CCE MC-1000`](https://bitbucket.org/lst_retro/system-software) machine.
* `TRS-80 Model I` support is planned to be implemented as soon as I get my dirty pawns on this machine. :-)
* Add `CPC` support is something being asked from a long time, but I'm unsure if I will be able to accomplish that without some serious help from CPC users.

## Development 

A classic "Versioned Releases" process is adopted by this project, with the versioning being tracked by Major Version, Minor Version and Build number using the format **major`.`minor`.`build**, where major, minor and build are growing up integers.

On the documentation, the release's state (alpha, beta, release candidate) can be attached to the build. Please be careful when putting these things into production - they should not be considered "stable".

* Major version:
	+ Major features that potentially changes the tool's behaviour, sometimes affecting legacy code, are issued with a new Major Version.
	+ Legacy code are expected (but not necessarily intended) to break.
	+ **Does not** migrate to a new Major version unless you are willing to verify your legacy code for compliance and/or you really need to use the new features.
		- If it's working, don't fix it. :-) 
	+ New Projects **should** adopt the last stable release, so you can use the new features! :-)
* Minor version:
	+ Small features and enhancements, that **are guaranteed** to do not break legacy code, are issued on minor versions.
	+ You **can** migrate to new minor versions without too much care, but yet sometimes legacy code could break due unintended colateral effects or new bugs
		- Please report such issues, **they need** to be fixed.  
* Build:
	+ No new features are issued on new builds, these releases are **only** for bug fixes.
	+ You **must** migrate to new builds, as it fixes bugs that will affect your (current or legacy) code **for sure**.
		- Off course, sometimes new bugs happens as I fix older ones, but so is the life... :-) 

**Alpha** Releases are terribly unstable, and don't respect the "don't change" rules from the last published release. Things are expected to change, brake or just vanish from the surface of the World. Use them at your own risk. :-)

**Beta** Releases are somewhat buggy, but they are stable. I do my best to do not deviate from the rules above. When it happens, it's a bug.

The [`master` branch](https://bitbucket.org/lst_retro/sjasmpp/) **is** the deliverable of the moment. It's also called "Production" on the documentation. Don't use anything else unless you feels that urge to suffer incommensurable and unholy pain. =D

See [`stage` branch](https://bitbucket.org/lst_retro/sjasmpp/branch/stage) to check on what I'm doing at the moment (also called "Development"). It's usually in a compilable status, but not necessarily usable. This branch is promoted to `master` when I'm satisfied with the current status and feel it's time to create a new deliverable. A `RELEASE` [tag](https://bitbucket.org/lst_retro/sjasmpp/commits/all#tags) is also issue when this happens.

Currently, the `dev` branch is my local copy from `stage`. I'm the only developer at this time, there's not need to a `dev` branch for now.

The `from` [branches](https://bitbucket.org/lst_retro/sjasmpp/commits/all#branches) are local mirrors from the upstreams, and the `merged` [branches](https://bitbucket.org/lst_retro/sjasmpp/commits/all#branches) are work branches already concluded and merged into `stage`.

For the sake of sanity, I'll keep the version numbering from the SjASMPlus fork.

### How do I get set up? ##

* For Linux
	* You will need Gnu's Core Utils and GCC.
* For MacOS
	* You will need Gnu Make and GCC. The Apple ones from X-Code do the job, but I use the newer versions from Mac Ports. 
* For Windows Desktop
	* mingw should do the trick. 
	* *Work in Progress* however.

You will need [ronn](http://rtomayko.github.io/ronn/) to build the man pages. The documentation is WiP at the moment.

## Some projects using it

* Apple //z [(link)](http://retro.lisias.net/my/projects/software/a2z/)
	+ A successful effort to run Z80 programs under ProDOS using the Microsoft SoftCard (and clones)!! :-)
		- Honest! I made it! :-)
* OpenApple Z Library [(link)](http://retro.lisias.net/my/projects/software/oazl/)
	+ A (not so) successful (yet) effort to use the AppliCard CP/M card (and its clones) as a fully featured multi-function co-processor card. 
* MC-1000 Firmware Replacement [(link)](https://bitbucket.org/lst_retro/mc1000-firmware)
	+ An ongoing effort to rewrite the MC-1000 Firmware, with enhancements, fixes e proper HW support.
	+ Includes BD.OS (Blue Drive OS)
* MC-1000 System Software [(link)](https://bitbucket.org/lst_retro/mc-1000-system-software)
	+ A myriad of device drivers, utilities and code snippets for the MC-1000.   

## History

Initially, the [SjASMPlus Z80 Cross Assembler Tool](https://sourceforge.net/projects/sjasmplus/) from My(?) Aprisobal <aprisobal@tut.by> was adopted in my projects. However, some pesky little bugs started to bite my \*\*\* and I felt the need to fix them myself (as nobody else was going to do that at that time).

As I discovered later, Aprisobal forked the code before the v0.39h version from Mastijn. So it lacks some features from that version.

The [fork owned by Néstor Soriano (konamiman)](https://github.com/Konamiman/Sjasm) is also being tracked. (Yes, **that** [konamiman](http://www.konamiman.com/msx/msx-e.html#nextor)!). For the sake of completude, [here is the old repository](https://bitbucket.org/konamiman/sjasm), not maintained anymore.

All of these forks are pretty outdated, however.

Recently, I found two efforts to revive the tool: [Vitamin Caig](https://github.com/vitamin-caig/sjasmplus)'s and [Michael Koloberdin](https://github.com/mkoloberdin/sjasmplus)'s, both forked directly from Aprisobal's.

Vitamin's fork fixed a lot of bugs, cleanup some code, modularized it a bit and added [mingw](http://www.mingw.org/) (Windows) support, and these features were front-ported to my fork. However, not all of his changes were ported as I aim to keep support for legacy SjASM dependent code.

So, yeah - I Cherry-Picked only the commits that mattered to me. :)

While cherry-picking, I realized that Vitamin's has forked the Koloberdin's fork and then had a pull request accepted by the latter. So I assumed that Vitamin's fork was intended to help Koloberdin on some tasks, and turned my attention to this new one.

(I'm glad he didn't deleted his repository - I would not be able to cherry-pick the changes I want!)

I decided to do not merge Koloberdin's work, however. He apparently wants to bring the tool to a modern development environment without caring for legacy support - he's currently refactoring the code to use C++ STDLIB (a wise move, as since Aprisobal's was making heavy use of C++ and there's no hope to come back to an ANSI C code-tree anymore) and [BOOST](http://www.boost.org/).

This last move doesn't appeals me however, as my intended target platforms will probably have C++ and STDLIB supported, but not necessarily Boost! And, speaking frankly, I don't see many features on Boost that would really make any difference on this project -  C++ and STDLIB should be enough for the foreseeable future.

Such refactoring also rendered almost impossible the task to front port the original's code base missing new features.

All these forks come from (in a way or another) the excellent [SjASM](http://www.xl2s.tk) from Sjoerd Mastijn (yes, **that** [guy](https://www.youtube.com/watch?v=W_X6Q1zfyN4)), and I'm aiming to keep legacy code working **perfectly** on this fork. 

Since all the forks mentioned (including mine!) derived in a way or another from Mastijn's code base, and he issued v0.39h(partially merged) V0.42c (not yet) with some interesting new features since them, these features will be front ported to SjASM++ also.

However, at the present state, there's no hope of merging back any code to the Grand Parent project or any other fork. All these code-trees are deviating fiercely from each other, as the project's goals are inexorably diverging.

## (External) Documentation

* Original [Documentation](http://www.xl2s.tk/sjasmmanual.html) (V0.42c)
* Some new [Documentation](https://github.com/mkoloberdin/sjasmplus/wiki) (mkoloberdin fork)
* Legacy [documentation](https://htmlpreview.github.io/?https://github.com/mkoloberdin/sjasmplus/blob/master/docs-html/index.html) (HTML converted from Docbook XML)
* Aprisobal's [Development Log](http://zx-pk.ru/threads/447-sjasmplus-z80-kross-assembler.html) (Russian)

## Who do I talk to? ##

* Lisias 
	* ***_retro at lisias dot net_*** for personal contacts, suggestions, help offerings and chitchat. :-)
	* ***_support at lisias dot net_*** for support and bug report. Add SjASM++ to the Subject line, please.
* Social Media
	* [Google+](https://plus.google.com/communities/115940022023483738521)
* I kindly ask you to do not bother the other project's maintainers with any bugs you found in this codebase. There's a good chance that I was the fscking idiot who created them. =D 

Please don't ask support or open bug reports on my personal account, neither use the support account for personal contacts. There's a reason I made two accounts to deal with this. ;-)

Please understand that contacting me about this project using any other meanings will be ignored.


# Other forks

* Michael Koloberdin
	+ https://github.com/mkoloberdin/sjasmplus.git
* vitamin-caig 
	+ https://github.com/vitamin-caig/sjasmplus.git
* My(?) Aprisobal
	+ https://sourceforge.net/p/sjasmplus/code/?source=navbar
* konamiman
	+ https://github.com/Konamiman/Sjasm.git
* The Original, Sjoerd Mastijn's SjASM
	+ http://www.xl2s.tk 

And recently, I also found:

* https://github.com/z00m128/sjasmplus
* https://github.com/pipagerardo/sjasmpg
* https://github.com/samsaga2/sjasm

note: There's no hope on merging to upstream for the foreseeable future. The "upstream repository" concept is not applicable anymore to this project.
