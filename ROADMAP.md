# SjASM++ Road Map

This is the planned development map:

* 1.8
	+ Fixing all known bugs
	+ Merging SjASM 0.39h and 0.42c features.
	+ Fixing the new bugs that will inevitably arise.
	+ Define and freeze the command line interface.
		- and Default options!	 
	+ Guarantee full compatibility to legacy code from SjASM and SjASMPlus, even by using command line options to enable and disable features.
* 1.9
	+ Define and freeze a common set of directives.
		- SjASM specific ones will be supported by "legacy" options.
	+ Define and freeze a proper HW support layer
		- Revise and freeze ZX Spectrum support
		- Add MC-1000 support
		- Add ZX81 and ZX80 support
		- Add MSX support 
	+ Write proper documentation.
* 2.x and beyond
	+ Define and freeze a rich set of numeric and math expressions
	+ Add CPU Support for:
		- 8080
		- 8085
		- 8088 / 8086 ?
			- 80286 (??)
		- 68000 (?)
		- 6502
			- 65816 (?)
		- 6809 / 6309 (?)
	+ Add HW support for
		- CPC
		- Apple II+ , //e 
			- IIgs & III ?
		- others?
