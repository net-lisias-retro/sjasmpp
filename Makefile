# Makefile for sjasmplus created by Tygrys' hands.

GCC?=gcc
CC?=$(GCC)
GPP?=g++
C++?=$(GPP)

EXE=sjasmpp
PREFIX=~/.local

CFLAGS=-O2 -Ilua5.1 -Itolua++ -I ./sjasm
SYS=$(shell $(GCC) -dumpmachine)
ifneq (,$(findstring darwin, $(SYS)))
	CFLAGS+=-DLUA_USE_MACOSX -DMAX_PATH=PATH_MAX 
	LDFLAGS=-ldl
else
ifneq (,$(findstring mingw, $(SYS)))
	EXE:=$(EXE).exe
	CFLAGS+= -DMAX_PATH=260 -DWINDOWS_MINGW -D_CRT_NONSTDC_NO_DEPRECATE -D_GNU_SOURCE
	LDFLAGS=--enable-static
else
	CFLAGS+=-DLUA_USE_LINUX -DMAX_PATH=1024
	LDFLAGS=-Wl,--no-as-needed -ldl
endif
endif

#sjasmplus object files
OBJS=\
	sjasm/sjasm.o \
		sjasm/sjerr.o sjasm/defines.o sjasm/directives.o sjasm/labels.o sjasm/modules.o sjasm/options.o \
		sjasm/parser.o sjasm/reader.o sjasm/sjio.o sjasm/support.o sjasm/tables.o \
	sjasm/modes/sjasmplus/main.o \
		sjasm/modes/sjasm/directives.o sjasm/modes/sjasmplus/directives.o \
	sjasm/modes/compass/main.o \
		sjasm/modes/compass/directives.o sjasm/modes/compass/lexical.o sjasm/modes/compass/syntax.o \
	sjasm/lua/main.o \
		sjasm/lua/bindings.o sjasm/lua/syntax.o sjasm/lua/directives.o sjasm/lua/lpack.o \
	sjasm/hw/devices/zx/spectrum/main.o \
		sjasm/hw/devices/zx/spectrum/lexical.o sjasm/hw/devices/zx/spectrum/directives.o\
		sjasm/hw/devices/zx/spectrum/io_snapshot.o sjasm/hw/devices/zx/spectrum/io_tape.o sjasm/hw/devices/zx/spectrum/io_trd.o \
	sjasm/hw/cpu/main.o \
		sjasm/hw/cpu/z80.o \
	sjasm/VERSION.o

#liblua objects
LUAOBJS= lua5.1/lapi.o lua5.1/lauxlib.o lua5.1/lbaselib.o lua5.1/lcode.o lua5.1/ldblib.o \
	lua5.1/ldebug.o lua5.1/ldo.o lua5.1/ldump.o lua5.1/lfunc.o lua5.1/lgc.o lua5.1/linit.o \
	lua5.1/liolib.o lua5.1/llex.o lua5.1/lmathlib.o lua5.1/lmem.o lua5.1/loadlib.o \
	lua5.1/lobject.o lua5.1/lopcodes.o lua5.1/loslib.o lua5.1/lparser.o lua5.1/lstate.o \
	lua5.1/lstring.o lua5.1/lstrlib.o lua5.1/ltable.o lua5.1/ltablib.o lua5.1/ltm.o \
	lua5.1/lundump.o lua5.1/lvm.o lua5.1/lzio.o

# tolua objects
TOLUAOBJS=tolua++/tolua_event.o tolua++/tolua_is.o tolua++/tolua_map.o \
	tolua++/tolua_push.o tolua++/tolua_to.o

GARBAGE_TYPES         := *.d *.o
DIRECTORIES_TO_CLEAN  := $(shell find . -not -path "./.git**" -type d)
GARBAGE_TYPED_FOLDERS := $(foreach DIR, $(DIRECTORIES_TO_CLEAN), $(addprefix $(DIR)/,$(GARBAGE_TYPES)))

TODAY=$(shell date +%Y-%m-%d)
COPYRIGHT_YEAR=$(shell date +%y)

all : $(EXE)

$(EXE): $(LUAOBJS) $(TOLUAOBJS) $(OBJS)
	$(GPP) -o $(EXE) $(LDFLAGS) $(CXXFLAGS) $(OBJS) $(LUAOBJS) $(TOLUAOBJS)

sjasm/VERSION.c:	VERSION.cc
	cat VERSION.cc | sed s/\\[PRODUCT_BUILT_AT\\]/$(TODAY)/ | sed s/\\[COPYRIGHT_YEAR\\]/$(COPYRIGHT_YEAR)/ > sjasm/VERSION.c

.c.o:
	$(GCC) $(CFLAGS) -o $@ -c $< -MMD
.cpp.o:
	$(GPP) -std=c++11 $(CFLAGS) -o $@ -c $< -MMD

clean:
	echo $(EXE)
	echo $(GARBAGE_TYPED_FOLDERS)
	$(RM) -rf $(GARBAGE_TYPED_FOLDERS)
	$(RM) sjasm/VERSION.cc

install: $(EXE)
	cp $(EXE) $(PREFIX)/bin/
    
include $(wildcard sjasm/*.d lua5.1/*.d tolua++/*.d)
