sjasmpp(1) -- SjASM++ an advanced Macro Assembler for retro-computers
=====================================================================

## SYNOPSIS

`sjasmpp` [\<options\>...] source\_file... 


## DESCRIPTION

WiP


## OPTIONS

The command line options *must* preced the source files list.

 * `--inc=`\<dir\> or `-i`\<dir\> or `-I`\<dir\> :
   Directories to be added into the include directories list. Use one option for directory, include how many you need.
    
 * `--default-cpu=`<cpuname> :
   Selects the default CPU used for building.
   The current options are:
      Z80            : Selects Z80 code generator; *DEFAULT*
      Z80-STRICT     : Only real Z80 Instructions are allowed;
      Z80-DOCUMENTED : Only documented Instructions are allowed;
   
 * `--sjasmplus-mode` :
   Enables SjASMPlus Compatibility Mode.
   It's needed to guarantee SjASMPlus legacy code to build, by enabling SjASMPlus directives and search path for Includes.

WiP


## RETURN VALUES

The tool returns positive Exit Codes for errors usually associated to development time, and are fixable (or workarounded) on the source code.

In a Nutshell:

* **negative** values are used for operating errors (something that must be fixed by the user compiling the code), usually fixable on the Makefile or calling process/batch file.
* **positive** values are used for coding or configuration errors (something that must be fixed by the developer), and usually needs source code editing to fix.
* **zero** means Success as usual.

The Exit Codes returned by the tool are:

* **00** - No error
* **01** - Internal Error. You should not see one of these. Please file a bug report. 
* **02** - Building process was interrupted by errors in the source code.
* **03** - A needed input file was found, but an error happend while reading it.
* **04** - A needed input file could not be opened. Check the code base. Check the 'checkout' consistency.
* **-1** - The building process was interrupted by a runtime error (as out of memory, out of disk space, etc).
* **-2** - No input files were found on the command line. Check Makefile.
* **-3** - Error on the command line - one or more options are invalid.
* **-4** - A file specified by the operator could not be written into. Check disk space. Check if the directory tree exists. Check permissions.

## AUTHOR

Lisias T http://retro.lisias.net/my/library/dev/sjasmpp.md

## COPYRIGHT

2016-2018 [Lisias T](http://lisias.net)

## SUPPORT

See the project's [homepage](http://retro.lisias.net/my/library/dev/sjasmpp.md) or [code repository](https://bitbucket.org/lst_retro/sjasmpp/)

## SEE ALSO

[SjASMPlus](https://sourceforge.net/projects/sjasmplus/) , [SjASM](http://www.xl2s.tk)
