# Change Log

* 2018-02-21 : Lisias <retro@lisias.net> [Version 1.8 alpha 4](https://bitbucket.org/lst_retro/sjasmpp/commits/tag/RELEASE/2018-0221.1_8_alpha4)
	+ This brach was promoted to **MASTER** after **two** years! :-)
		- All **my** legacy code is proven to compile (correcly) (with the `--sjasmplus-mode` option enabled)
	+ Some code semantic errors fixed
	+ the spurious "[SIZE] Multiple sizes?" error was fixed.
		- The make process won't wrongly abort anymore. #HURRAY!
	+ the annoying "Bytes lost" gratuitous warnings were fixed.
	+ Some code conformance and bug preventing changes.
	+ MINGW is now functional. Windows is back to the menu.
		- MSC still unmaintained, and should stay this way for some time.
	+ Fixing INCLUDE search order, as ANSI C does
		- <file> search include path first, current later.
		- "file" search current path first, include later.
	+ SjASMPlus Compatibility mode is now disabled by default.
		- --sjasmplus-mode on the command line to enable it.
		- It rollbacks the include file search order to "current dir first" no matter what.
	+ New commandline for CPU support.
		- Deleting the "nofakes" option
		- adding --default-cpu
			- Z80, Z80-STRICT & Z80-DOCUMENTED options.  

* 2018-02-20 : Lisias <retro@lisias.net> - [Version 1.8 alpha 3](https://bitbucket.org/lst_retro/sjasmpp/commits/tag/RELEASE/2018-0220.1_8_alpha3)
	+ some merging (and possibly just plain dumbness) errors fixed
	+ A bit more refactoring for modularization and decoupling.
	+ This release works! I managed to rebuild the MC-1000 Firmware!! #HURRAY!
	+ This is the last release to maintain **full** SjASMPlus compatibility (including operating bugs) by default.

* 2018-02-19 : Lisias <retro@lisias.net> - [Version 1.8 alpha 2](https://bitbucket.org/lst_retro/sjasmpp/commits/tag/RELEASE/2018-0219.1_8_alpha2)
	+ Heavy refactoring to promote modulatization and easy maintenance
	+ (re)syncing with 0.38h started
	+ no bug fixes
	+ no regression testing! Use at your own risk!

* 2018-02-11 : Lisias <retro@lisias.net>
	+ Updating and fixing Copyright notices
	+ Merged updates from https://github.com/vitamin-caig/sjasmplus

* 2017-03-11 : Michael Koloberdin <koloberdin@gmail.com>
	+ CR+LF line-endings processing

* 2016-04-04 : Lisias  <retro@lisias.net> - [Version 1.08 alpha 0](https://bitbucket.org/lst_retro/sjasmpp/commits/tag/RELEASE/2016-0508.1_08_alpha0)
	+ Forking project from https://github.com/mkoloberdin/sjasmplus.git
	+ Renaming it to SJASM++ (or sjasmpp)
	+ Adding TODO list

* 2008-04-03 : Aprisobal  <my@aprisobal.by>
	+ VERSION 1.07 Stable
	+ Added new SAVETAP pseudo-op. It's support up to 1024kb ZX-Spectrum's RAM.
	+ Added new --nofakes commandline parameter.

* 2008-04-02 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC7
	+ Another fix of 48k SNA snapshots saving routine.
	+ Added new UNDEFINE pseudo-op.
	+ Added new IFUSED/IFNUSED pseudo-ops for labels (such IFDEF for defines).
	+ Fixed labels list dump rountine (--lstlab command line parameter).

* 2008-03-29 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC6
	+ Applied bugfix patches for SAVEHOB/SAVETRD pseudo-ops by Breeze.
	+ Fixed memory leak in line parsing routine.
	+ Fixed 48k SNA snapshots saving routine.
	+ Added missing INF instruction.
	+ Fixed code parser's invalid addressing of temporary labels in macros.

* 2007-05-31 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC5bf
	+ Bugfix patches by Ric Hohne.
	+ Important bugfix of memory leak.
	+ Bugfix of strange crashes at several machines.
	+ Added yet another sample for built-in LUA engine. See end of this file.
	+ Added sources of CosmoCubes demo to the "examples" directory.

* 2007-05-13 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC5
	+ ALIGN has new optional parameter.
	+ Corrected bug of RAM sizing.
	+ Corrected bug of structures naming.

* 2006-12-02 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC4bf
	+ Corrected important bug in code generation functions of SjASMPlus.

* 2006-11-28 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC4
	+ Corrected bug with SAVEBIN, SAVETRD and possible SAVESNA.
	+ Add Makefile to build under Linux, FreeBSD etc.

* 2006-10-12 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC3
	+ SAVESNA can save 48kb snapshots
	+ Corrected DEFINE's bug.
	+ Corrected bug of incorrect line numbering.

* 2006-09-28 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC2
	+ SAVESNA works and with device ZXSPECTRUM48
	+ Added new device PENTAGON128
	+ In ZXSPECTRUM48 device and others attributes has black ink and white paper by default.

* 2006-09-23 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC1bf
	+ Corrected bug with _ERRORS and _WARNINGS constants
	+ Added error message, when SHELLEXEC program execution failed

* 2006-09-18 : Aprisobal  <my@aprisobal.by>
	+ VERSION: 1.07 RC1
	+ 3-pass design
	+ Built-in Lua scripting engine
	+ Changed command line keys
	+ Documentation converted to HTML.
	+ Added new directives: DEVICE, SLOT, SHELLEXEC
	+ Added predefined constanst: _SJASMPLUS=1, _ERRORS and other
	+ Changed output log format.
	+ And many many more.

* New in 0.39g6 : Sjoerd Mastijn <sjasm@xl2s.tk>
	+ : Operator.
	+ MULUB and MULUW work.
	+ Labels starting with an underscore are listed.
	+ Can't remember the other changes.

* New in 0.39g1 : Sjoerd Mastijn <sjasm@xl2s.tk>
	+ Sjasm now allows spaces in filenames.

* New in 0.39g : Sjoerd Mastijn <sjasm@xl2s.tk>
	+ ENDMAP directive.
	+ DEFM and DM synonyms for BYTE.
	+ Some bug fixes:
		- file size is reset when a new output file is opened.
		- 'bytes lost' warning fixed.
	+ And thanks to Konamiman:
		- PHASE and DEPHASE directives as synonyms of TEXTAREA and ENDT.
		- FPOS directive.
		- Expanded OUTPUT directive.
		- The possibility to generate a symbol file.

* Changes from 0.30 : Sjoerd Mastijn <sjasm@xl2s.tk>
	+ '#' Can be used now to indicate a hexadecimal value.
	+ Local 'number' labels should really work now.
	+ Multiple error messages per source line possible.
	+ Things like ld a,(4)+1 work again (=> ld a,5). Somehow I broke this in v0.3.
	+ Filenames don't need surrounding quotes anymore.
	+ Macro's can be nested once again.
	+ Better define-handling.
	+ 'Textarea' detects forward references.
	+ Include within include searches relative to that include.
	+ 'Unlocked' some directives (assert, output).
	+ '#' Can be used in labels.
	+ No space needed between label and '=' in statements like 'label=value'.
	+ The 'export' directive now exports 'label: EQU value' instead of 'label = value'.
	+ Labelnames starting with a register name (like HL_kip) shouldn't confuse Sjasm anymore.
	+ RLC, RRC, RL, RR, SLA, SRA, SLL (SLI), RES and SET undocumented instructions added.
	+ "ex af,af" and "ex af" are possible now.
	+ Added defb, defw and defs.
	+ Locallabels now also work correctly when used in macros.
	+ Added // and limited /* */ comments.
	+ Sjasm now checks the label values between passes.
	+ '>>>' Operator added.
	+ Sources included.
	+ '>>>' And '>>' operators now work the way they should (I think).
	+ Removed the 'data/text/pool'-bug. (Together with the data/text/pool directives. ~8)
	+ Added endmodule and endmod to end a module.
	+ STRUCT directive.
	+ REPT, DWORD, DD, DEFD directives.
	+ More freedom with character constants; in some cases it's possible to use double quotes...
	+ It's now possible to specify include paths at the command line.
	+ Angle brackets are used now to include commas in macroarguments. So check your macro's.
	+ Fixed a structure initialization bug.
	+ It's not possible to use CALLs or jumps without arguments anymore.
	+ DZ and ABYTEZ directives to define null terminated strings.
	+ Sjasm now checks for lines that are too long.
	+ Added '16 bit' LD, LDI and LDD 'instructions'. (See end of this text:)
	+ PUSH and POP accept a list of registers.
	+ Added '16 bit SUB' instruction.
	+ Unreferenced labels are indicated with an 'X' in the label listing.
	+ Unknown escapecodes in strings result in just one error (instead of more).
	+ Labelnameparts now have a maximum of 70 characters.
	+ Improved IX and IY offset checking.
	+ Maximum, minimum, and, or, mod, xor, not, shl, shr, low and high operators added.
	+ Logical operations result in -1 or 0 instead of 1 or 0. Of course, -1 for true and 0 for false ;)
	+ Fixed the 'ifdef <illegal identifier>' bug. (:

* Changes from 0.2 : Sjoerd Mastijn <sjasm@xl2s.tk>
	+ Sjasm v0.3x assumes Z80-mode as there are no other cpus supported.
	+ All calculations are 32 bits.
